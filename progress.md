# CPU

## Completed

* Intial setup
* Instruction helpers
* Instruction calls

## To Do

* Interrupts

# MMU

## Completed

* Intial setup
* Registers
* BIOS
* ROM Bank 0
* VRAM
* ERAM
* WRAM
* OAM
* Zero Page

## To Do

* ROM Bank 1 switching
* IO
* Timers

# GPU

## Completed

* Initial setup
* Initial tile rendering (untested)

## To Do

* Use GPU flags
* Verify tile rendering
* Window rendering
* Sprite rendering

# Audio

Not started

# Input

Not started