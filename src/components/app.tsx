import React from "react";
import { JSBoy } from "../emulator";
import {
    GPU_FLAG_BG_ENABLED,
    GPU_FLAG_SPRITES_ENABLED,
    GPU_FLAG_SPRITE_SIZE,
    GPU_FLAG_BG_TILE_MAP,
    GPU_FLAG_BG_TILE_SET,
    GPU_FLAG_WINDOW_ENABLED,
    GPU_FLAG_WINDOW_TILE_MAP,
    GPU_FLAG_DISPLAY_ENABLED
} from "../emulator/gpu";
import { LogLevel } from "../emulator/logger";
import { formatHex } from "../emulator/util";
import DebugPanel from "./debug-panel";
import Player from "./player";
import Controls from "./controls";

const ROM_OPTIONS = [
    { label: "CPU Instruction Test", value: "/roms/cpu_instrs.gb" },
    { label: "Instruction Timing Test", value: "/roms/instr_timing.gb" },
    { label: "Memory Timing Test", value: "/roms/mem_timing.gb" },
    { label: "Interrupt Time Test", value: "/roms/interrupt_time.gb" },
    { label: "CGB Sound Test", value: "/roms/cgb_sound.gb" },
    { label: "DMG Sound Test", value: "/roms/dmg_sound.gb" },
    { label: "OAM Bug Test", value: "/roms/oam_bug.gb" },
    { label: "Tetris", value: "/roms/tetris.gb" } 
];

interface AppState {
    startEnabled: boolean;
    selectedROM: string;
    debug: boolean;
}

export default class App extends React.Component<{}, AppState> {
    emulator = null;

    constructor(props) {
        super(props);
        this.state = {
            startEnabled: false,
            selectedROM: ROM_OPTIONS[0].value,
            debug: true
        };
    }

    onCanvasRef = (canvas?: HTMLCanvasElement) => {
        if (canvas) {
            this.emulator = new JSBoy(canvas, {
                logLevel: LogLevel.INFO,
                logHistoryDepth: 5000
            });
            this.emulator.loadROM(this.state.selectedROM).then(() => {
                this.setState({ startEnabled: true });
            });
            window["jsboy"] = this.emulator;
        } else {
            this.emulator = null;
            window["jsboy"] = null;
        }
    };

    selectROM = (rom: string) => {
        this.setState({ startEnabled: false, selectedROM: rom });
        this.emulator.loadROM(rom).then(() => {
            this.setState({ startEnabled: true });
        });
    };

    toggleDebug = () => {
        this.setState({ debug: !this.state.debug });
    };

    start = () => {
        this.emulator.start();
    };

    pause = () => {
        this.emulator.pause();
    };

    reset = () => {
        this.emulator.reset();
    };

    render() {
        return (
            <div className="app">
                {this.state.debug ? <DebugPanel /> : null}
                <Player innerRef={this.onCanvasRef} />
                <Controls
                    romOptions={ROM_OPTIONS}
                    selectedROM={this.state.selectedROM}
                    selectROM={this.selectROM}
                    debug={this.state.debug}
                    toggleDebug={this.toggleDebug}
                    startEnabled={this.state.startEnabled}
                    start={this.start}
                    pause={this.pause}
                    reset={this.reset}
                />
            </div>
        );
    }
}