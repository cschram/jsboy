import React from "react";
import "./controls.css";

interface ControlsProps {
    romOptions: Array<{
        label: string;
        value: string;
    }>;
    selectedROM: string;
    selectROM(rom: string): void;
    debug: boolean;
    toggleDebug(): void;
    startEnabled: boolean;
    start(): void;
    pause(): void;
    reset(): void;
}

export default function Controls({
    romOptions,
    selectedROM,
    selectROM,
    debug,
    toggleDebug,
    startEnabled,
    start,
    pause,
    reset
}: ControlsProps) {
    const onChangeROM = (e) => {
        selectROM(e.target.value);
    };
    return (
        <div className="controls">
            <select value={selectedROM} onChange={onChangeROM}>
                {romOptions.map(romOption =>
                    <option key={romOption.value} value={romOption.value}>{romOption.label}</option>
                )}
            </select>
            <label>
                <input
                    type="checkbox"
                    checked={debug}
                    onChange={toggleDebug}
                />
                <span>Debug</span>
            </label>
            <div>
                <button disabled={!startEnabled} onClick={start}>Start</button>
                <button onClick={pause}>Pause</button>
                <button onClick={reset}>Reset</button>
            </div>
        </div>
    );
}