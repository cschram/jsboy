import React from "react";
import "./debug-panel.css";

export default function DebugPanel() {
    return (
        <div className="debug-panel">
            <h1 id="debug-title">Debug Panel</h1>
            <label>
                <input id="debug-pause-frame" type="checkbox" />
                <span>Pause each frame</span>
            </label>
            <div className="debug-panel__section">
                <h2>Registers</h2>
                <div className="debug-panel__register">
                    <span>A:</span>
                    <span id="debug-register-a">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>F:</span>
                    <span id="debug-register-f">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>B:</span>
                    <span id="debug-register-b">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>C:</span>
                    <span id="debug-register-c">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>D:</span>
                    <span id="debug-register-d">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>E:</span>
                    <span id="debug-register-e">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>H:</span>
                    <span id="debug-register-h">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>L:</span>
                    <span id="debug-register-l">0x00</span>
                </div>
                <div className="debug-panel__register">
                    <span>SP:</span>
                    <span id="debug-register-sp">0x0000</span>
                </div>
                <div className="debug-panel__register">
                    <span>PC:</span>
                    <span id="debug-register-pc">0x0000</span>
                </div>
            </div>
            <div className="debug-panel__section">
                <h2>GPU</h2>
                <h3>Flags</h3>
                <div className="debug-panel__gpu-flag">
                    <span>Background Enabled:</span>
                    <span id="debug-gpu-flag-bg-enabled">No</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Sprites Enabled:</span>
                    <span id="debug-gpu-flag-sprites-enabled">No</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Sprite Size:</span>
                    <span id="debug-gpu-flag-sprite-size">8x8</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Background Tile Map:</span>
                    <span id="debug-gpu-flag-bg-tile-map">0</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Background Tile Set:</span>
                    <span id="debug-gpu-flag-bg-tile-set">0</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Window Enabled:</span>
                    <span id="debug-gpu-flag-window-enabled">No</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Window Tile Map:</span>
                    <span id="debug-gpu-flag-window-tile-map">0</span>
                </div>
                <div className="debug-panel__gpu-flag">
                    <span>Display Enabled:</span>
                    <span id="debug-gpu-flag-display-enabled">No</span>
                </div>
                <h3>Tile Set 0</h3>
                <canvas id="debug-tileset-0" width="256" height="256"></canvas>
                <h3>Tile Set 1</h3>
                <canvas id="debug-tileset-1" width="256" height="256"></canvas>
            </div>
        </div>
    );
}