import React from "react";
import "./player.css";

interface PlayerProps {
    innerRef(canvas?: HTMLCanvasElement): void;
}

export default function Player({ innerRef }: PlayerProps) {
    return (
        <div className="player">
            <canvas ref={innerRef}></canvas>
            <h1>JS Boy</h1>
        </div>
    );
}