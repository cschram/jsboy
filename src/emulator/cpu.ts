import { JSBoy } from "./index";
import { Logger, LogEntry } from "./logger";
import { MMU } from "./mmu";
import { instructions } from "./instructions";
import { Registers } from "./registers";
import { formatHex } from "./util";

type InstructionStats = {
    [name: string]: number;
}

export class CPU {
    public halted: boolean;
    private emulator: JSBoy;
    private logger: Logger;
    private clock: number;
    private instructionStats: InstructionStats;

    constructor(emulator: JSBoy) {
        this.emulator = emulator;
        const { logLevel, logHistoryDepth } = emulator.getOptions();
        this.logger = new Logger("CPU", logLevel, logHistoryDepth);
        this.reset();
    };

    public getLogHistory(): Array<LogEntry> {
        return this.logger.getHistory();
    }

    public clearLogHistory() {
        this.logger.clearHistory();
    }

    public getInstructionStats(): InstructionStats {
        return this.instructionStats;
    }

    public getClock(): number {
        return this.clock;
    }

    public reset() {
        this.clearLogHistory();
        this.halted = false;
        this.clock = 0;
        this.instructionStats = {};
    }

    public step() {
        const instruction = this.mmu.readByte(this.registers.pc);
        this.registers.pc++;
        if (instructions[instruction]) {
            const [
                name,
                operandLength,
                clock,
                operation
            ] = instructions[instruction];
            let operand = 0;
            if (operandLength > 0) {
                if (operandLength === 1) {
                    operand = this.mmu.readByte(this.registers.pc);
                } else if (operandLength === 2) {
                    operand = this.mmu.readWord(this.registers.pc);
                }
                this.logger.debug(`[${formatHex(this.registers.pc - 1)}] ${name} (${formatHex(operand)})`);
            } else {
                this.logger.debug(`[${formatHex(this.registers.pc - 1)}] ${name}`);
            }
            this.registers.t = clock;
            operation(this, this.mmu, operand);
            this.registers.pc += operandLength;
            this.clock += this.registers.t;
            if (name in this.instructionStats) {
                this.instructionStats[name]++;
            } else {
                this.instructionStats[name] = 1;
            }
        } else {
            throw new Error(`Unknown/unimplemented instruction ${formatHex(instruction)} at address ${formatHex(this.registers.pc - 1)}`);
        }
    };

    private get mmu(): MMU {
        return this.emulator.getMMU();
    }

    private get registers(): Registers {
        return this.mmu.registers;
    }
}