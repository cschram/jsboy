import { MMU } from "./mmu";
import {
    rotate_left_carry,
    rotate_right_carry,
    rotate_left,
    rotate_right,
    shift_left_sign,
    shift_right_sign,
    shift_right,
    swap,
    test_bit,
    set_bit
} from "./util";

export type ExtendedInstruction = [
    string, // name
    number, // cycles
    (mmu: MMU) => void // instruction function
];

export const extendedInstructions: Array<ExtendedInstruction> = [
    ["RLC B", 2, rlc_b],
    ["RLC C", 2, rlc_c],
    ["RLC D", 2, rlc_d],
    ["RLC E", 2, rlc_e],
    ["RLC H", 2, rlc_h],
    ["RLC L", 2, rlc_l],
    ["RLC (HL)", 4, rlc_hlp],
    ["RLC A", 2, rlc_a],
    ["RRC B", 2, rrc_b],
    ["RRC C", 2, rrc_c],
    ["RRC D", 2, rrc_d],
    ["RRC E", 2, rrc_e],
    ["RRC H", 2, rrc_h],
    ["RRC L", 2, rrc_l],
    ["RRC (HL)", 4, rrc_hlp],
    ["RRC A", 2, rrc_a],
    ["RL B", 2, rl_b],
    ["RL C", 2, rl_c],
    ["RL D", 2, rl_d],
    ["RL E", 2, rl_e],
    ["RL H", 2, rl_h],
    ["RL L", 2, rl_l],
    ["RL (HL)", 4, rl_hlp],
    ["RL A", 2, rl_a],
    ["RR B", 2, rr_b],
    ["RR C", 2, rr_c],
    ["RR D", 2, rr_d],
    ["RR E", 2, rr_e],
    ["RR H", 2, rr_h],
    ["RR L", 2, rr_l],
    ["RR (HL)", 4, rr_hlp],
    ["RR A", 2, rr_a],
    ["SLA B", 2, sla_b],
    ["SLA C", 2, sla_c],
    ["SLA D", 2, sla_d],
    ["SLA E", 2, sla_e],
    ["SLA H", 2, sla_h],
    ["SLA L", 2, sla_l],
    ["SLA (HL)", 4, sla_hlp],
    ["SLA A", 2, sla_a],
    ["SRA B", 2, sra_b],
    ["SRA C", 2, sra_c],
    ["SRA D", 2, sra_d],
    ["SRA E", 2, sra_e],
    ["SRA H", 2, sra_h],
    ["SRA L", 2, sra_l],
    ["SRA (HL)", 4, sra_hlp],
    ["SRA A", 2, sra_a],
    ["SWAP B", 2, swap_b],
    ["SWAP C", 2, swap_c],
    ["SWAP D", 2, swap_d],
    ["SWAP E", 2, swap_e],
    ["SWAP H", 2, swap_h],
    ["SWAP L", 2, swap_l],
    ["SWAP (HL)", 4, swap_hlp],
    ["SWAP A", 2, swap_a],
    ["SRL B", 2, srl_b],
    ["SRL C", 2, srl_c],
    ["SRL D", 2, srl_d],
    ["SRL E", 2, srl_e],
    ["SRL H", 2, srl_h],
    ["SRL L", 2, srl_l],
    ["SRL (HL)", 4, srl_hlp],
    ["SRL A", 2, srl_a],
    ["BIT 0, B", 2, bit_0_b],
    ["BIT 0, C", 2, bit_0_c],
    ["BIT 0, D", 2, bit_0_d],
    ["BIT 0, E", 2, bit_0_e],
    ["BIT 0, H", 2, bit_0_h],
    ["BIT 0, L", 2, bit_0_l],
    ["BIT 0, (HL)", 3, bit_0_hlp],
    ["BIT 0, A", 2, bit_0_a],
    ["BIT 1, B", 2, bit_1_b],
    ["BIT 1, C", 2, bit_1_c],
    ["BIT 1, D", 2, bit_1_d],
    ["BIT 1, E", 2, bit_1_e],
    ["BIT 1, H", 2, bit_1_h],
    ["BIT 1, L", 2, bit_1_l],
    ["BIT 1, (HL)", 3, bit_1_hlp],
    ["BIT 1, A", 2, bit_1_a],
    ["BIT 2, B", 2, bit_2_b],
    ["BIT 2, C", 2, bit_2_c],
    ["BIT 2, D", 2, bit_2_d],
    ["BIT 2, E", 2, bit_2_e],
    ["BIT 2, H", 2, bit_2_h],
    ["BIT 2, L", 2, bit_2_l],
    ["BIT 2, (HL)", 3, bit_2_hlp],
    ["BIT 2, A", 2, bit_2_a],
    ["BIT 3, B", 2, bit_3_b],
    ["BIT 3, C", 2, bit_3_c],
    ["BIT 3, D", 2, bit_3_d],
    ["BIT 3, E", 2, bit_3_e],
    ["BIT 3, H", 2, bit_3_h],
    ["BIT 3, L", 2, bit_3_l],
    ["BIT 3, (HL)", 3, bit_3_hlp],
    ["BIT 3, A", 2, bit_3_a],
    ["BIT 4, B", 2, bit_4_b],
    ["BIT 4, C", 2, bit_4_c],
    ["BIT 4, D", 2, bit_4_d],
    ["BIT 4, E", 2, bit_4_e],
    ["BIT 4, H", 2, bit_4_h],
    ["BIT 4, L", 2, bit_4_l],
    ["BIT 4, (HL)", 3, bit_4_hlp],
    ["BIT 4, A", 2, bit_4_a],
    ["BIT 5, B", 2, bit_5_b],
    ["BIT 5, C", 2, bit_5_c],
    ["BIT 5, D", 2, bit_5_d],
    ["BIT 5, E", 2, bit_5_e],
    ["BIT 6, H", 2, bit_5_h],
    ["BIT 6, L", 2, bit_5_l],
    ["BIT 5, (HL)", 3, bit_5_hlp],
    ["BIT 5, A", 2, bit_5_a],
    ["BIT 6, B", 2, bit_6_b],
    ["BIT 6, C", 2, bit_6_c],
    ["BIT 6, D", 2, bit_6_d],
    ["BIT 6, E", 2, bit_6_e],
    ["BIT 6, H", 2, bit_6_h],
    ["BIT 6, L", 2, bit_6_l],
    ["BIT 6, (HL)", 3, bit_6_hlp],
    ["BIT 6, A", 2, bit_6_a],
    ["BIT 7, B", 2, bit_7_b],
    ["BIT 7, C", 2, bit_7_c],
    ["BIT 7, D", 2, bit_7_d],
    ["BIT 7, E", 2, bit_7_e],
    ["BIT 7, H", 2, bit_7_h],
    ["BIT 7, L", 2, bit_7_l],
    ["BIT 7, (HL)", 3, bit_7_hlp],
    ["BIT 7, A", 2, bit_7_a],
    ["RES 0, B", 2, res_0_b],
    ["RES 0, C", 2, res_0_c],
    ["RES 0, D", 2, res_0_d],
    ["RES 0, E", 2, res_0_e],
    ["RES 0, H", 2, res_0_h],
    ["RES 0, L", 2, res_0_l],
    ["RES 0, (HL)", 3, res_0_hlp],
    ["RES 0, A", 2, res_0_a],
    ["RES 1, B", 2, res_1_b],
    ["RES 1, C", 2, res_1_c],
    ["RES 1, D", 2, res_1_d],
    ["RES 1, E", 2, res_1_e],
    ["RES 1, H", 2, res_1_h],
    ["RES 1, L", 2, res_1_l],
    ["RES 1, (HL)", 3, res_1_hlp],
    ["RES 1, A", 2, res_1_a],
    ["RES 2, B", 2, res_2_b],
    ["RES 2, C", 2, res_2_c],
    ["RES 2, D", 2, res_2_d],
    ["RES 2, E", 2, res_2_e],
    ["RES 2, H", 2, res_2_h],
    ["RES 2, L", 2, res_2_l],
    ["RES 2, (HL)", 3, res_2_hlp],
    ["RES 2, A", 2, res_2_a],
    ["RES 3, B", 2, res_3_b],
    ["RES 3, C", 2, res_3_c],
    ["RES 3, D", 2, res_3_d],
    ["RES 3, E", 2, res_3_e],
    ["RES 3, H", 2, res_3_h],
    ["RES 3, L", 2, res_3_l],
    ["RES 3, (HL)", 3, res_3_hlp],
    ["RES 3, A", 2, res_3_a],
    ["RES 4, B", 2, res_4_b],
    ["RES 4, C", 2, res_4_c],
    ["RES 4, D", 2, res_4_d],
    ["RES 4, E", 2, res_4_e],
    ["RES 4, H", 2, res_4_h],
    ["RES 4, L", 2, res_4_l],
    ["RES 4, (HL)", 3, res_4_hlp],
    ["RES 4, A", 2, res_4_a],
    ["RES 5, B", 2, res_5_b],
    ["RES 5, C", 2, res_5_c],
    ["RES 5, D", 2, res_5_d],
    ["RES 5, E", 2, res_5_e],
    ["RES 5, H", 2, res_5_h],
    ["RES 5, L", 2, res_5_l],
    ["RES 5, (HL)", 3, res_5_hlp],
    ["RES 5, A", 2, res_5_a],
    ["RES 6, B", 2, res_6_b],
    ["RES 6, C", 2, res_6_c],
    ["RES 6, D", 2, res_6_d],
    ["RES 6, E", 2, res_6_e],
    ["RES 6, H", 2, res_6_h],
    ["RES 6, L", 2, res_6_l],
    ["RES 6, (HL)", 3, res_6_hlp],
    ["RES 6, A", 2, res_6_a],
    ["RES 7, B", 2, res_7_b],
    ["RES 7, C", 2, res_7_c],
    ["RES 7, D", 2, res_7_d],
    ["RES 7, E", 2, res_7_e],
    ["RES 7, H", 2, res_7_h],
    ["RES 7, L", 2, res_7_l],
    ["RES 7, (HL)", 3, res_7_hlp],
    ["RES 7, A", 2, res_7_a],
    ["SET 0, B", 2, set_0_b],
    ["SET 0, C", 2, set_0_c],
    ["SET 0, D", 2, set_0_d],
    ["SET 0, E", 2, set_0_e],
    ["SET 0, H", 2, set_0_h],
    ["SET 0, L", 2, set_0_l],
    ["SET 0, (HL)", 3, set_0_hlp],
    ["SET 0, A", 2, set_0_a],
    ["SET 1, B", 2, set_1_b],
    ["SET 1, C", 2, set_1_c],
    ["SET 1, D", 2, set_1_d],
    ["SET 1, E", 2, set_1_e],
    ["SET 1, H", 2, set_1_h],
    ["SET 1, L", 2, set_1_l],
    ["SET 1, (HL)", 3, set_1_hlp],
    ["SET 1, A", 2, set_1_a],
    ["SET 2, B", 2, set_2_b],
    ["SET 2, C", 2, set_2_c],
    ["SET 2, D", 2, set_2_d],
    ["SET 2, E", 2, set_2_e],
    ["SET 2, H", 2, set_2_h],
    ["SET 2, L", 2, set_2_l],
    ["SET 2, (HL)", 3, set_2_hlp],
    ["SET 2, A", 2, set_2_a],
    ["SET 3, B", 2, set_3_b],
    ["SET 3, C", 2, set_3_c],
    ["SET 3, D", 2, set_3_d],
    ["SET 3, E", 2, set_3_e],
    ["SET 3, H", 2, set_3_h],
    ["SET 3, L", 2, set_3_l],
    ["SET 3, (HL)", 3, set_3_hlp],
    ["SET 3, A", 2, set_3_a],
    ["SET 4, B", 2, set_4_b],
    ["SET 4, C", 2, set_4_c],
    ["SET 4, D", 2, set_4_d],
    ["SET 4, E", 2, set_4_e],
    ["SET 4, H", 2, set_4_h],
    ["SET 4, L", 2, set_4_l],
    ["SET 4, (HL)", 3, set_4_hlp],
    ["SET 4, A", 2, set_4_a],
    ["SET 5, B", 2, set_5_b],
    ["SET 5, C", 2, set_5_c],
    ["SET 5, D", 2, set_5_d],
    ["SET 5, E", 2, set_5_e],
    ["SET 5, H", 2, set_5_h],
    ["SET 5, L", 2, set_5_l],
    ["SET 5, (HL)", 3, set_5_hlp],
    ["SET 5, A", 2, set_5_a],
    ["SET 6, B", 2, set_6_b],
    ["SET 6, C", 2, set_6_c],
    ["SET 6, D", 2, set_6_d],
    ["SET 6, E", 2, set_6_e],
    ["SET 6, H", 2, set_6_h],
    ["SET 6, L", 2, set_6_l],
    ["SET 6, (HL)", 3, set_6_hlp],
    ["SET 6, A", 2, set_6_a],
    ["SET 7, B", 2, set_7_b],
    ["SET 7, C", 2, set_7_c],
    ["SET 7, D", 2, set_7_d],
    ["SET 7, E", 2, set_7_e],
    ["SET 7, H", 2, set_7_h],
    ["SET 7, L", 2, set_7_l],
    ["SET 7, (HL)", 3, set_7_hlp],
    ["SET 7, A", 2, set_7_a]
];

export function rlc_b(mmu: MMU) {
    mmu.registers.b = rotate_left_carry(mmu, mmu.registers.b);
}

export function rlc_c(mmu: MMU) {
    mmu.registers.c = rotate_left_carry(mmu, mmu.registers.c);
}

export function rlc_d(mmu: MMU) {
    mmu.registers.d = rotate_left_carry(mmu, mmu.registers.d);
}

export function rlc_e(mmu: MMU) {
    mmu.registers.e = rotate_left_carry(mmu, mmu.registers.e);
}

export function rlc_h(mmu: MMU) {
    mmu.registers.h = rotate_left_carry(mmu, mmu.registers.h);
}

export function rlc_l(mmu: MMU) {
    mmu.registers.l = rotate_left_carry(mmu, mmu.registers.l);
}

export function rlc_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, rotate_left_carry(mmu, mmu.readByte(mmu.registers.hl)));
}

export function rlc_a(mmu: MMU) {
    mmu.registers.a = rotate_left_carry(mmu, mmu.registers.a, false);
}

export function rrc_b(mmu: MMU) {
    mmu.registers.b = rotate_right_carry(mmu, mmu.registers.b);
}

export function rrc_c(mmu: MMU) {
    mmu.registers.c = rotate_right_carry(mmu, mmu.registers.c);
}

export function rrc_d(mmu: MMU) {
    mmu.registers.d = rotate_right_carry(mmu, mmu.registers.d);
}

export function rrc_e(mmu: MMU) {
    mmu.registers.e = rotate_right_carry(mmu, mmu.registers.e);
}

export function rrc_h(mmu: MMU) {
    mmu.registers.h = rotate_right_carry(mmu, mmu.registers.h);
}

export function rrc_l(mmu: MMU) {
    mmu.registers.l = rotate_right_carry(mmu, mmu.registers.l);
}

export function rrc_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, rotate_right_carry(mmu, mmu.readByte(mmu.registers.hl)));
}

export function rrc_a(mmu: MMU) {
    mmu.registers.a = rotate_right_carry(mmu, mmu.registers.a, false);
}

export function rl_b(mmu: MMU) {
    mmu.registers.b = rotate_left(mmu, mmu.registers.b);
}

export function rl_c(mmu: MMU) {
    mmu.registers.c = rotate_left(mmu, mmu.registers.c);
}

export function rl_d(mmu: MMU) {
    mmu.registers.d = rotate_left(mmu, mmu.registers.d);
}

export function rl_e(mmu: MMU) {
    mmu.registers.e = rotate_left(mmu, mmu.registers.e);
}

export function rl_h(mmu: MMU) {
    mmu.registers.h = rotate_left(mmu, mmu.registers.h);
}

export function rl_l(mmu: MMU) {
    mmu.registers.l = rotate_left(mmu, mmu.registers.l);
}

export function rl_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, rotate_left(mmu, mmu.readByte(mmu.registers.hl)));
}

export function rl_a(mmu: MMU) {
    mmu.registers.a = rotate_left(mmu, mmu.registers.a, false);
}

export function rr_b(mmu: MMU) {
    mmu.registers.b = rotate_right(mmu, mmu.registers.b);
}

export function rr_c(mmu: MMU) {
    mmu.registers.c = rotate_right(mmu, mmu.registers.c);
}

export function rr_d(mmu: MMU) {
    mmu.registers.d = rotate_right(mmu, mmu.registers.d);
}

export function rr_e(mmu: MMU) {
    mmu.registers.e = rotate_right(mmu, mmu.registers.e);
}

export function rr_h(mmu: MMU) {
    mmu.registers.h = rotate_right(mmu, mmu.registers.h);
}

export function rr_l(mmu: MMU) {
    mmu.registers.l = rotate_right(mmu, mmu.registers.l);
}

export function rr_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, rotate_right(mmu, mmu.readByte(mmu.registers.hl)));
}

export function rr_a(mmu: MMU) {
    mmu.registers.a = rotate_right(mmu, mmu.registers.a, false);
}

export function sla_b(mmu: MMU) {
    mmu.registers.b = shift_left_sign(mmu, mmu.registers.b);
}

export function sla_c(mmu: MMU) {
    mmu.registers.c = shift_left_sign(mmu, mmu.registers.c);
}

export function sla_d(mmu: MMU) {
    mmu.registers.d = shift_left_sign(mmu, mmu.registers.d);
}

export function sla_e(mmu: MMU) {
    mmu.registers.e = shift_left_sign(mmu, mmu.registers.e);
}

export function sla_h(mmu: MMU) {
    mmu.registers.h = shift_left_sign(mmu, mmu.registers.h);
}

export function sla_l(mmu: MMU) {
    mmu.registers.l = shift_left_sign(mmu, mmu.registers.l);
}

export function sla_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, shift_left_sign(mmu, mmu.readByte(mmu.registers.hl)));
}

export function sla_a(mmu: MMU) {
    mmu.registers.a = shift_left_sign(mmu, mmu.registers.a);
}

export function sra_b(mmu: MMU) {
    mmu.registers.b = shift_right_sign(mmu, mmu.registers.b);
}

export function sra_c(mmu: MMU) {
    mmu.registers.c = shift_right_sign(mmu, mmu.registers.c);
}

export function sra_d(mmu: MMU) {
    mmu.registers.d = shift_right_sign(mmu, mmu.registers.d);
}

export function sra_e(mmu: MMU) {
    mmu.registers.e = shift_right_sign(mmu, mmu.registers.e);
}

export function sra_h(mmu: MMU) {
    mmu.registers.h = shift_right_sign(mmu, mmu.registers.h);
}

export function sra_l(mmu: MMU) {
    mmu.registers.l = shift_right_sign(mmu, mmu.registers.l);
}

export function sra_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, shift_right_sign(mmu, mmu.registers.hl));
}

export function sra_a(mmu: MMU) {
    mmu.registers.a = shift_right_sign(mmu, mmu.registers.a);
}

export function swap_b(mmu: MMU) {
    mmu.registers.b = swap(mmu, mmu.registers.b);
}

export function swap_c(mmu: MMU) {
    mmu.registers.c = swap(mmu, mmu.registers.c);
}

export function swap_d(mmu: MMU) {
    mmu.registers.d = swap(mmu, mmu.registers.d);
}

export function swap_e(mmu: MMU) {
    mmu.registers.e = swap(mmu, mmu.registers.e);
}

export function swap_h(mmu: MMU) {
    mmu.registers.h = swap(mmu, mmu.registers.h);
}

export function swap_l(mmu: MMU) {
    mmu.registers.l = swap(mmu, mmu.registers.l);
}

export function swap_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, swap(mmu, mmu.readByte(mmu.registers.hl)));
}

export function swap_a(mmu: MMU) {
    mmu.registers.a = swap(mmu, mmu.registers.a);
}

export function srl_b(mmu: MMU) {
    mmu.registers.b = shift_right(mmu, mmu.registers.b);
}

export function srl_c(mmu: MMU) {
    mmu.registers.c = shift_right(mmu, mmu.registers.c);
}

export function srl_d(mmu: MMU) {
    mmu.registers.d = shift_right(mmu, mmu.registers.d);
}

export function srl_e(mmu: MMU) {
    mmu.registers.e = shift_right(mmu, mmu.registers.e);
}

export function srl_h(mmu: MMU) {
    mmu.registers.h = shift_right(mmu, mmu.registers.h);
}

export function srl_l(mmu: MMU) {
    mmu.registers.l = shift_right(mmu, mmu.registers.l);
}

export function srl_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, shift_right(mmu, mmu.readByte(mmu.registers.hl)));
}

export function srl_a(mmu: MMU) {
    mmu.registers.a = shift_right(mmu, mmu.registers.a);
}

export function bit_0_b(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.b);
}

export function bit_0_c(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.c);
}

export function bit_0_d(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.d);
}

export function bit_0_e(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.e);
}

export function bit_0_h(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.h);
}

export function bit_0_l(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.l);
}

export function bit_0_hlp(mmu: MMU) {
    test_bit(mmu, 0, mmu.readByte(mmu.registers.hl));
}

export function bit_0_a(mmu: MMU) {
    test_bit(mmu, 0, mmu.registers.a);
}

export function bit_1_b(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.b);
}

export function bit_1_c(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.c);
}

export function bit_1_d(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.d);
}

export function bit_1_e(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.e);
}

export function bit_1_h(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.h);
}

export function bit_1_l(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.l);
}

export function bit_1_hlp(mmu: MMU) {
    test_bit(mmu, 1, mmu.readByte(mmu.registers.hl));
}

export function bit_1_a(mmu: MMU) {
    test_bit(mmu, 1, mmu.registers.a);
}

export function bit_2_b(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.b);
}

export function bit_2_c(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.c);
}

export function bit_2_d(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.d);
}

export function bit_2_e(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.e);
}

export function bit_2_h(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.h);
}

export function bit_2_l(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.l);
}

export function bit_2_hlp(mmu: MMU) {
    test_bit(mmu, 2, mmu.readByte(mmu.registers.hl));
}

export function bit_2_a(mmu: MMU) {
    test_bit(mmu, 2, mmu.registers.a);
}

export function bit_3_b(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.b);
}

export function bit_3_c(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.c);
}

export function bit_3_d(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.d);
}

export function bit_3_e(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.e);
}

export function bit_3_h(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.h);
}

export function bit_3_l(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.l);
}

export function bit_3_hlp(mmu: MMU) {
    test_bit(mmu, 3, mmu.readByte(mmu.registers.hl));
}

export function bit_3_a(mmu: MMU) {
    test_bit(mmu, 3, mmu.registers.a);
}

export function bit_4_b(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.b);
}

export function bit_4_c(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.c);
}

export function bit_4_d(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.d);
}

export function bit_4_e(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.e);
}

export function bit_4_h(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.h);
}

export function bit_4_l(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.l);
}

export function bit_4_hlp(mmu: MMU) {
    test_bit(mmu, 4, mmu.readByte(mmu.registers.hl));
}

export function bit_4_a(mmu: MMU) {
    test_bit(mmu, 4, mmu.registers.a);
}

export function bit_5_b(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.b);
}

export function bit_5_c(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.c);
}

export function bit_5_d(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.d);
}

export function bit_5_e(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.e);
}

export function bit_5_h(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.h);
}

export function bit_5_l(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.l);
}

export function bit_5_hlp(mmu: MMU) {
    test_bit(mmu, 5, mmu.readByte(mmu.registers.hl));
}

export function bit_5_a(mmu: MMU) {
    test_bit(mmu, 5, mmu.registers.a);
}

export function bit_6_b(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.b);
}

export function bit_6_c(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.c);
}

export function bit_6_d(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.d);
}

export function bit_6_e(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.e);
}

export function bit_6_h(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.h);
}

export function bit_6_l(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.l);
}

export function bit_6_hlp(mmu: MMU) {
    test_bit(mmu, 6, mmu.readByte(mmu.registers.hl));
}

export function bit_6_a(mmu: MMU) {
    test_bit(mmu, 6, mmu.registers.a);
}

export function bit_7_b(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.b);
}

export function bit_7_c(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.c);
}

export function bit_7_d(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.d);
}

export function bit_7_e(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.e);
}

export function bit_7_h(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.h);
}

export function bit_7_l(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.l);
}

export function bit_7_hlp(mmu: MMU) {
    test_bit(mmu, 7, mmu.readByte(mmu.registers.hl));
}

export function bit_7_a(mmu: MMU) {
    test_bit(mmu, 7, mmu.registers.a);
}

export function res_0_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 0);
}

export function res_0_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 0);
}

export function res_0_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 0);
}

export function res_0_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 0);
}

export function res_0_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 0);
}

export function res_0_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 0);
}

export function res_0_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.readByte(mmu.registers.hl) & ~(1 << 0));
}

export function res_0_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 0);
}

export function res_1_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 1);
}

export function res_1_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 1);
}

export function res_1_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 1);
}

export function res_1_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 1);
}

export function res_1_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 1);
}

export function res_1_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 1);
}

export function res_1_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 1));
}

export function res_1_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 1);
}

export function res_2_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 2);
}

export function res_2_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 2);
}

export function res_2_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 2);
}

export function res_2_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 2);
}

export function res_2_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 2);
}

export function res_2_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 2);
}

export function res_2_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 2));
}

export function res_2_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 2);
}

export function res_3_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 3);
}

export function res_3_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 3);
}

export function res_3_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 3);
}

export function res_3_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 3);
}

export function res_3_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 3);
}

export function res_3_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 3);
}

export function res_3_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 3));
}

export function res_3_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 3);
}

export function res_4_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 4);
}

export function res_4_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 4);
}

export function res_4_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 4);
}

export function res_4_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 4);
}

export function res_4_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 4);
}

export function res_4_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 4);
}

export function res_4_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 4));
}

export function res_4_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 4);
}

export function res_5_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 5);
}

export function res_5_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 5);
}

export function res_5_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 5);
}

export function res_5_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 5);
}

export function res_5_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 5);
}

export function res_5_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 5);
}

export function res_5_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 5));
}

export function res_5_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 5);
}

export function res_6_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 6);
}

export function res_6_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 6);
}

export function res_6_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 6);
}

export function res_6_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 6);
}

export function res_6_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 6);
}

export function res_6_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 6);
}

export function res_6_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.hl & ~(1 << 6));
}

export function res_6_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 6);
}

export function res_7_b(mmu: MMU) {
    mmu.registers.b &= ~(1 << 7);
}

export function res_7_c(mmu: MMU) {
    mmu.registers.c &= ~(1 << 7);
}

export function res_7_d(mmu: MMU) {
    mmu.registers.d &= ~(1 << 7);
}

export function res_7_e(mmu: MMU) {
    mmu.registers.e &= ~(1 << 7);
}

export function res_7_h(mmu: MMU) {
    mmu.registers.h &= ~(1 << 7);
}

export function res_7_l(mmu: MMU) {
    mmu.registers.l &= ~(1 << 7);
}

export function res_7_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, mmu.readByte(mmu.registers.hl) & ~(1 << 7));
}

export function res_7_a(mmu: MMU) {
    mmu.registers.a &= ~(1 << 7);
}

export function set_0_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 0, mmu.registers.b);
}

export function set_0_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 0, mmu.registers.c);
}

export function set_0_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 0, mmu.registers.d);
}

export function set_0_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 0, mmu.registers.e);
}

export function set_0_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 0, mmu.registers.h);
}

export function set_0_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 0, mmu.registers.l);
}

export function set_0_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 0, mmu.readByte(mmu.registers.hl)));
}

export function set_0_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 0, mmu.registers.a);
}

export function set_1_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 1, mmu.registers.b);
}

export function set_1_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 1, mmu.registers.c);
}

export function set_1_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 1, mmu.registers.d);
}

export function set_1_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 1, mmu.registers.e);
}

export function set_1_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 1, mmu.registers.h);
}

export function set_1_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 1, mmu.registers.l);
}

export function set_1_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 1, mmu.readByte(mmu.registers.hl)));
}

export function set_1_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 1, mmu.registers.a);
}

export function set_2_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 2, mmu.registers.b);
}

export function set_2_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 2, mmu.registers.c);
}

export function set_2_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 2, mmu.registers.d);
}

export function set_2_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 2, mmu.registers.e);
}

export function set_2_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 2, mmu.registers.h);
}

export function set_2_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 2, mmu.registers.l);
}

export function set_2_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 2, mmu.readByte(mmu.registers.hl)));
}

export function set_2_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 2, mmu.registers.a);
}

export function set_3_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 3, mmu.registers.b);
}

export function set_3_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 3, mmu.registers.c);
}

export function set_3_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 3, mmu.registers.d);
}

export function set_3_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 3, mmu.registers.e);
}

export function set_3_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 3, mmu.registers.h);
}

export function set_3_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 3, mmu.registers.l);
}

export function set_3_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 3, mmu.readByte(mmu.registers.hl)));
}

export function set_3_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 3, mmu.registers.a);
}

export function set_4_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 4, mmu.registers.b);
}

export function set_4_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 4, mmu.registers.c);
}

export function set_4_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 4, mmu.registers.d);
}

export function set_4_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 4, mmu.registers.e);
}

export function set_4_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 4, mmu.registers.h);
}

export function set_4_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 4, mmu.registers.l);
}

export function set_4_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 4, mmu.readByte(mmu.registers.hl)));
}

export function set_4_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 4, mmu.registers.a);
}

export function set_5_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 5, mmu.registers.b);
}

export function set_5_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 5, mmu.registers.c);
}

export function set_5_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 5, mmu.registers.d);
}

export function set_5_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 5, mmu.registers.e);
}

export function set_5_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 5, mmu.registers.h);
}

export function set_5_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 5, mmu.registers.l);
}

export function set_5_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 5, mmu.readByte(mmu.registers.hl)));
}

export function set_5_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 5, mmu.registers.a);
}

export function set_6_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 6, mmu.registers.b);
}

export function set_6_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 6, mmu.registers.c);
}

export function set_6_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 6, mmu.registers.d);
}

export function set_6_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 6, mmu.registers.e);
}

export function set_6_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 6, mmu.registers.h);
}

export function set_6_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 6, mmu.registers.l);
}

export function set_6_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 6, mmu.readByte(mmu.registers.hl)));
}

export function set_6_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 6, mmu.registers.a);
}

export function set_7_b(mmu: MMU) {
    mmu.registers.b = set_bit(mmu, 7, mmu.registers.b);
}

export function set_7_c(mmu: MMU) {
    mmu.registers.c = set_bit(mmu, 7, mmu.registers.c);
}

export function set_7_d(mmu: MMU) {
    mmu.registers.d = set_bit(mmu, 7, mmu.registers.d);
}

export function set_7_e(mmu: MMU) {
    mmu.registers.e = set_bit(mmu, 7, mmu.registers.e);
}

export function set_7_h(mmu: MMU) {
    mmu.registers.h = set_bit(mmu, 7, mmu.registers.h);
}

export function set_7_l(mmu: MMU) {
    mmu.registers.l = set_bit(mmu, 7, mmu.registers.l);
}

export function set_7_hlp(mmu: MMU) {
    mmu.writeByte(mmu.registers.hl, set_bit(mmu, 7, mmu.readByte(mmu.registers.hl)));
}

export function set_7_a(mmu: MMU) {
    mmu.registers.a = set_bit(mmu, 7, mmu.registers.a);
}