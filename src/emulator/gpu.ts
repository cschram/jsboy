import { JSBoy } from "./index";
import { Logger, LogLevel, LogEntry } from "./logger";
import { MMU } from "./mmu";
import { Registers } from "./registers";

export const SCREEN_WIDTH = 160;
export const SCREEN_HEIGHT = 144;

export type Color = [number, number, number];
export type Palette = [number, number, number, number];

export const GPU_FLAG_BG_ENABLED = 0x01;
export const GPU_FLAG_SPRITES_ENABLED = 0x02;
export const GPU_FLAG_SPRITE_SIZE = 0x04;
export const GPU_FLAG_BG_TILE_MAP = 0x08;
export const GPU_FLAG_BG_TILE_SET = 0x10;
export const GPU_FLAG_WINDOW_ENABLED = 0x20;
export const GPU_FLAG_WINDOW_TILE_MAP = 0x40;
export const GPU_FLAG_DISPLAY_ENABLED = 0x80;

export interface Sprite {
    id: number;
    x: number;
    y: number;
    tile: number;
    palette: number;
    xFlip: boolean;
    yFlip: boolean;
    priority: boolean;
}

export const BASE_PALETTE: Array<Color> = [
    [255, 255, 255],
    [192, 192, 192],
    [96, 96, 96],
    [0, 0, 0]
];

export enum GPUMode {
    HBLANK,
    VBLANK,
    OAM_READ,
    VRAM_READ
}

export type Tileset = number[][][];

export class GPU {
    private emulator: JSBoy;
    private logger: Logger;
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private framebuffer: Uint8Array;
    private mode: GPUMode;
    private flags: number;
    private scrollX: number;
    private scrollY: number;
    private scanline: number;
    private tileset: Tileset;
    private tilesetDirty: [boolean, boolean];
    private bgPalette: Palette;
    private spritePalette: Array<Palette>;
    private sprites: Array<Sprite>;
    private sortedSprites: Array<Sprite>;
    private clock: number;

    constructor(emulator: JSBoy) {
        this.emulator = emulator;
        const { logLevel, logHistoryDepth } = emulator.getOptions();
        this.logger = new Logger("GPU", logLevel, logHistoryDepth);
        this.canvas = emulator.getCanvas();
        this.context = this.canvas.getContext("2d");
        this.reset();
    }

    public get mmu(): MMU {
        return this.emulator.getMMU();
    }

    public get registers(): Registers {
        return this.mmu.registers;
    }

    public reset() {
        this.clearLogHistory();
        this.canvas.width = SCREEN_WIDTH;
        this.canvas.height = SCREEN_HEIGHT;
        this.framebuffer = new Uint8Array(SCREEN_WIDTH * SCREEN_HEIGHT * 4);
        this.mode = GPUMode.HBLANK;
        this.clock = 0;
        this.scrollX = 0;
        this.scrollY = 0;
        this.scanline = 0;
        this.bgPalette = [0, 1, 2, 3];
        this.spritePalette = [
            [0, 1, 2, 3],
            [0, 1, 2, 3]
        ];
        this.tileset = [];
        for (let i = 0; i < 512; i++) {
            this.tileset[i] = [];
            for (let j = 0; j < 8; j++) {
                this.tileset[i][j] = [];
                for (let k = 0; k < 8; k++) {
                    this.tileset[i][j][k] = 0;
                }
            }
        }
        this.tilesetDirty = [true, true];
        this.sprites = [];
        this.sortedSprites = [];
        for (let i = 0; i < 40; i++) {
            this.sprites[i] = {
                id: i,
                x: -8,
                y: -16,
                tile: 0,
                palette: 0,
                xFlip: false,
                yFlip: false,
                priority: false
            };
            this.sortedSprites[i] = this.sprites[i];
        }
        this.clearScreen();
    }

    public getLogHistory(): Array<LogEntry> {
        return this.logger.getHistory();
    }

    public clearLogHistory() {
        this.logger.clearHistory();
    }

    public flagIsSet(flag: number): boolean {
        return !!(this.flags & flag);
    }

    public setFlag(flag: number) {
        this.flags |= flag;
    }

    public clearFlag(flag: number) {
        this.flags &= ~flag;
    }

    public clearScreen() {
        this.context.fillStyle = `rgb(${BASE_PALETTE[0][0]}, ${BASE_PALETTE[0][1]}, ${BASE_PALETTE[0][2]})`;
        this.context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }

    public getPixel(x: number, y: number): Color {
        const index = (x * 4) + (y * SCREEN_HEIGHT * 4);
        return [this.framebuffer[index], this.framebuffer[index + 1], this.framebuffer[index + 2]];
    }

    public setPixel(x: number, y: number, color: Color) {
        const index = (x * 4) + (y * SCREEN_WIDTH * 4);
        this.framebuffer[index] = color[0];
        this.framebuffer[index + 1] = color[1];
        this.framebuffer[index + 2] = color[2];
        this.framebuffer[index + 3] = 255;
    }

    public updateVRAM(address: number, vram: Uint8Array) {
        const addr = address & 1 ? address - 1 : address;
        const tile = (addr >> 4) & 511;
        const y = (addr >> 1) & 7;
        for (let x = 0; x < 8; x++) {
            const sx = 1 << (7 - x);
            this.tileset[tile][y][x] = ((vram[addr] & sx) ? 1 : 0) | ((vram[addr + 1] & sx) ? 2 : 0);
        }
        this.tilesetDirty[tile > 255 ? 1 : 0] = true;
    }

    public updateOAM(address: number, value: number) {
        const id = address >> 2;
        if (id < 40) {
            switch (address & 3) {
                case 0:
                    this.sprites[id].y = value - 16;
                    break;

                case 1:
                    this.sprites[id].x = value - 8;
                    break;

                case 2:
                    if (this.flagIsSet(GPU_FLAG_SPRITE_SIZE)) {
                        this.sprites[id].tile = value & 0xFE;
                    } else {
                        this.sprites[id].tile = value;
                    }
                    break;

                case 3:
                    this.sprites[id].xFlip = !!(value & 0x20);
                    this.sprites[id].yFlip = !!(value & 0x40);
                    this.sprites[id].palette = value & 0x10 ? 1 : 0;
                    this.sprites[id].priority = !!(value & 0x80);
                    break;
            }
            this.sortedSprites = this.sprites.slice(0).sort((a, b) => {
                if (a.x > b.x) {
                    return -1;
                }
                if (a.id > b.id) {
                    return -1;
                }
                return 0;
            });
        }
    }

    public getFlags(): number {
        return this.flags;
    }

    public setFlags(flags: number) {
        this.flags = flags;
        if (!this.flagIsSet(GPU_FLAG_DISPLAY_ENABLED)) {
            this.clearScreen();
        }
    }

    public getScrollX(): number {
        return this.scrollX;
    }

    public setScrollX(scrollX: number) {
        this.scrollX = scrollX;
    }

    public getScrollY(): number {
        return this.scrollY;
    }

    public setScrollY(scrollY: number) {
        this.scrollY = scrollY;
    }

    public getScanline(): number {
        return this.scanline;
    }

    public setBGPalette(value: number) {
        for (let i = 0; i < 4; i++) {
            this.bgPalette[i] = (value >> (i * 2)) & 3;
        }
    }

    public setSpritePalette(id: number, value: number) {
        if (id < 0 || id > 1) {
            throw new Error(`Invalid sprite palette ID ${id}`);
        }
        for (let i = 0; i < 4; i++) {
            this.spritePalette[id][i] = (value >> (i * 2)) & 3;
        }
    }

    public step() {
        this.clock += this.mmu.registers.t;

        switch (this.mode) {
            case GPUMode.HBLANK:
                if (this.clock >= 204) {
                    this.clock = 0;
                    this.scanline++;
                    if (this.scanline === SCREEN_HEIGHT) {
                        this.mode = GPUMode.VBLANK;
                        this.render();
                        this.logger.debug("vblank");
                    } else {
                        this.mode = GPUMode.OAM_READ;
                    }
                }
                break;

            case GPUMode.VBLANK:
                if (this.clock >= 456) {
                    this.clock = 0;
                    this.scanline++;
                    if (this.scanline >= 153) {
                        this.mode = GPUMode.OAM_READ;
                        this.scanline = 0;
                    }
                }
                break;

            case GPUMode.OAM_READ:
                if (this.clock >= 80) {
                    this.clock = 0;
                    this.mode = GPUMode.VRAM_READ;
                }
                break;

            case GPUMode.VRAM_READ:
                if (this.clock >= 172) {
                    this.clock = 0;
                    this.mode = GPUMode.HBLANK;
                    this.logger.debug("hblank");
                    this.renderLine();
                }
                break;
        }
    }

    public drawTileSet(id: number, context: CanvasRenderingContext2D) {
        if (this.tilesetDirty[id]) {
            const tilebase = id === 0 ? 0 : 256;
            context.fillStyle = `rgb(${BASE_PALETTE[0][0]}, ${BASE_PALETTE[0][1]}, ${BASE_PALETTE[0][2]})`;
            context.fillRect(0, 0, 256, 256);
            for (let tile = 0; tile < 256; tile++) {
                const tiledata = this.tileset[tilebase + tile];
                const tilex = (tile % 32) * 8;
                const tiley = Math.floor(tile / 32) * 8;
                for (let y = 0; y < 8; y++) {
                    const tilerow = tiledata[y];
                    for (let x = 0; x < 8; x++) {
                        const color = BASE_PALETTE[this.bgPalette[tilerow[x]]];
                        context.fillStyle = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
                        context.fillRect(tilex + x, tiley + y, 1, 1);
                    }
                }
            }
            this.tilesetDirty[id] = false;
        }
    }

    private getTile(address: number): number {
        const tile = this.mmu.readByte(0x8000 + address);
        if (!this.flagIsSet(GPU_FLAG_BG_TILE_SET)) {
            return tile + 256;
        }
        return tile;
    }

    private renderLine() {
        if (this.flagIsSet(GPU_FLAG_DISPLAY_ENABLED)) {
            if (this.flagIsSet(GPU_FLAG_BG_ENABLED)) {
                const bgOffset = this.flagIsSet(GPU_FLAG_BG_TILE_MAP) ? 0x1C00 : 0x1800;
                const mapOffset = bgOffset + ((((this.scanline + this.scrollY) & 255) >> 3) << 5);
                let lineOffset = (this.scrollX >> 3) & 31;
                let x = this.scrollX & 7;
                const y = (this.scanline + this.scrollY) & 7;
                let tile = this.getTile(mapOffset + lineOffset);
                let tilerow = this.tileset[tile][y];
                for (let i = 0; i < SCREEN_WIDTH; i++) {
                    const color = BASE_PALETTE[this.bgPalette[tilerow[x]]];
                    this.setPixel(i, this.scanline, color);
                    x++;
                    if (x === 8) {
                        x = 0;
                        lineOffset = (lineOffset + 1) & 31;
                        let tile = this.getTile(mapOffset + lineOffset);
                        tilerow = this.tileset[tile][y];
                    }
                }
            }
            if (this.flagIsSet(GPU_FLAG_SPRITES_ENABLED)) {

            }
        }
    }

    private render() {
        if (this.flagIsSet(GPU_FLAG_DISPLAY_ENABLED)) {
            for (let x = 0; x < SCREEN_WIDTH; x++) {
                for (let y = 0; y < SCREEN_HEIGHT; y++) {
                    const color = this.getPixel(x, y);
                    this.context.fillStyle = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
                    this.context.fillRect(x, y, 1, 1);
                }
            }
        }
    }
}