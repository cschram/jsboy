import * as EventEmitter from "eventemitter3";
import { CPU } from "./cpu";
import { GPU } from "./gpu";
import { Joypad } from "./joypad";
import { Logger, LogLevel, LogEntry } from "./logger";
import { MMU } from "./mmu";

export interface JSBoyOptions {
    logLevel: LogLevel;
    logHistoryDepth: number;
    scopeInputs: boolean;
}

export interface JSBoyParams {
    logLevel?: LogLevel;
    logHistoryDepth?: number;
    scopeInputs?: boolean;
}

const defaultOptions = {
    logLevel: LogLevel.WARN,
    logHistoryDepth: 1000,
    scopeInputs: false
};

export class JSBoy {
    public events: EventEmitter;
    private options: JSBoyOptions;
    private logger: Logger;
    private canvas: HTMLCanvasElement;
    private mmu: MMU;
    private cpu: CPU;
    private gpu: GPU;
    private joypad: Joypad;
    private rom: Uint8Array;
    private frame: number;

    constructor(canvas: HTMLCanvasElement, options?: JSBoyParams) {
        this.events = new EventEmitter.EventEmitter();
        this.options = Object.assign({}, defaultOptions, options);
        this.logger = new Logger("System", this.options.logLevel, this.options.logHistoryDepth);
        this.canvas = canvas;
        this.mmu = new MMU(this);
        this.cpu = new CPU(this);
        this.gpu = new GPU(this);
        this.joypad = new Joypad();
        this.frame = 0;

        if (this.options.scopeInputs) {
            canvas.addEventListener("keydown", (event: KeyboardEvent) => {
                event.preventDefault();
                this.joypad.keyDown(event);
            });
            canvas.addEventListener("keyup", (event: KeyboardEvent) => {
                event.preventDefault();
                this.joypad.keyUp(event);
            });
        } else {
            document.addEventListener("keydown", (event: KeyboardEvent) => {
                event.preventDefault();
                this.joypad.keyDown(event);
            });
            document.addEventListener("keyup", (event: KeyboardEvent) => {
                event.preventDefault();
                this.joypad.keyUp(event);
            });
        }
    }

    public getLogHistory(): Array<LogEntry> {
        return this.logger.getHistory();
    }

    public clearLogHistory() {
        this.logger.clearHistory();
    }

    public getMMU(): MMU {
        return this.mmu;
    }

    public getCPU(): CPU {
        return this.cpu;
    }

    public getGPU(): GPU {
        return this.gpu;
    }

    public getJoypad(): Joypad {
        return this.joypad;
    }

    public getOptions(): JSBoyOptions {
        return this.options;
    }

    public getCanvas(): HTMLCanvasElement {
        return this.canvas;
    }

    public async loadROM(uri: string) {
        this.logger.info(`Loading ROM ${uri}`);
        const response = await fetch(uri);
        this.rom = new Uint8Array(await response.arrayBuffer());
        this.mmu.loadROM(this.rom);
    }

    public start() {
        this.logger.info("Starting emulation");
        this.mmu.running = true;
        const loop = () => {
            this.frame++;
            this.logger.debug(`Frame ${this.frame}`);
            const fclock = this.cpu.getClock() + 17556;
            while (this.cpu.getClock() < fclock && this.mmu.running) {
                this.cpu.step();
                this.gpu.step();
            }
            if (this.mmu.running) {
                requestAnimationFrame(loop);
                this.events.emit("frame");
            }
        }
        this.events.emit("start");
        requestAnimationFrame(loop);
    }

    public pause() {
        this.logger.info("Pausing emulation");
        this.mmu.running = false;
        this.events.emit("pause");
    }

    public reset() {
        this.logger.info("Resetting emulation");
        this.clearLogHistory();
        this.frame = 0;
        this.mmu.running = false;
        this.mmu.reset();
        this.cpu.reset();
        this.gpu.reset();
        requestAnimationFrame(() => {
            this.mmu.loadROM(this.rom);
            this.start();
        });
    }
}