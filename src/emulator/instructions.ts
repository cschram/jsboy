import { CPU } from "./cpu";
import {
    MMU,
    FLAG_ZERO,
    FLAG_NEGATIVE,
    FLAG_HALFCARRY,
    FLAG_CARRY
} from "./mmu";
import {
    formatHex,
    signed,
    inc,
    dec,
    add_byte,
    add_word,
    add_carry_to_a,
    sub_carry_from_a,
    sub,
    and,
    or,
    xor,
    compare_a,
    rotate_left_carry,
    rotate_right_carry,
    rotate_left
} from "./util";
import { extendedInstructions } from "./extended-instructions";

export type Instruction = [
    string, // name
    number, // operand length
    number, // clock cycles
    (cpu: CPU, mmu: MMU, operand: number) => void // instruction function
];

export const instructions: Array<Instruction> = [
    // 0x00
    ["NOP", 0, 1, nop],
    ["LD BC,nn", 2, 3, ld_bc_nn],
    ["LD (BC),  A", 0, 2, ld_bcp_a],
    ["INC BC", 0, 1, inc_bc],
    ["INC B", 0, 1, inc_b],
    ["DEC B", 0, 1, dec_b],
    ["LD B,n", 1, 2, ld_b_n],
    ["RLCA", 0, 1, rlca],
    ["LD (nn),SP", 2, 2, ld_nnp_sp],
    ["ADD HL,BC", 0, 3, add_hl_bc],
    ["LD A,(BC)", 0, 2, ld_a_bcp],
    ["DEC BC", 0, 1, dec_bc],
    ["INC C", 0, 1, inc_c],
    ["DEC C", 0, 1, dec_c],
    ["LD C,n", 1, 2, ld_c_n],
    ["RRCA", 0, 1, rrca],
    // 0x10
    ["STOP", 1, 2, stop],
    ["LD DE,nn", 2, 3, ld_de_nn],
    ["LD (DE),A", 0, 2, ld_dep_a],
    ["INC DE", 0, 1, inc_de],
    ["INC D", 0, 1, inc_d],
    ["DEC D", 0, 2, dec_d],
    ["LD D, n", 1, 2, ld_d_n],
    ["RLA", 0, 4, rla],
    ["JR n", 1, 2, jr_n],
    ["ADD HL,DE", 0, 4, add_hl_de],
    ["LD A,(DE)", 0, 2, ld_a_dep],
    ["DEC DE", 0, 4, dec_de],
    ["INC E", 0, 1, inc_e],
    ["DEC E", 0, 2, dec_e],
    ["LD E,n", 1, 2, ld_e_n],
    ["RRA", 0, 1, rra],
    // 0x20
    ["JR NZ,n", 1, 0, jr_nz_n],
    ["LD HL,nn", 2, 3, ld_hl_nn],
    ["LDI (HL),A", 0, 2, ldi_hlp_a],
    ["INC HL", 0, 1, inc_hl],
    ["INC H", 0, 1, inc_h],
    ["DEC H", 0, 2, dec_h],
    ["LD H,n", 1, 2, ld_h_n],
    ["DAA", 0, 2, daa],
    ["JR Z,n", 1, 2, jr_z_n],
    ["ADD HL,HL", 0, 4, add_hl_hl],
    ["LDI A,(HL)", 0, 2, ldi_a_hlp],
    ["DEC HL", 0, 4, dec_hl],
    ["INC L", 0, 1, inc_l],
    ["DEC L", 0, 2, dec_l],
    ["LD L,n", 1, 2, ld_l_n],
    ["CPL", 0, 2, cpl],
    // 0x30
    ["JR NC,n", 1, 2, jr_nc_n],
    ["LD SP,nn", 2, 3, ld_sp_nn],
    ["LDD (HL),A", 0, 2, ldd_hlp_a],
    ["INC SP", 0, 1, inc_sp],
    ["INC (HL)", 0, 1, inc_hlp],
    ["DEC (HL)", 0, 1, dec_hlp],
    ["LD (HL),n", 1, 3, ld_hlp_n],
    ["SCF", 0, 1, scf],
    ["JR C,n", 1, 2, jr_c_n],
    ["ADD HL,SP", 0, 4, add_hl_sp],
    ["LDD A,(HL)", 0, 2, ldd_a_hlp],
    ["DEC SP", 0, 1, dec_sp],
    ["INC A", 0, 1, inc_a],
    ["DEC A", 0, 1, dec_a],
    ["LD A,n", 1, 2, ld_a_n],
    ["CCF", 0, 1, ccf],
    // 0x40
    ["LD B,B", 0, 1, nop],
    ["LD B,C", 0, 1, ld_b_c],
    ["LD B,D", 0, 1, ld_b_d],
    ["LD B,E", 0, 1, ld_b_e],
    ["LD B,H", 0, 1, ld_b_h],
    ["LD B,L", 0, 1, ld_b_l],
    ["LD B,(HL)", 0, 2, ld_b_hlp],
    ["LD B,A", 0, 1, ld_b_a],
    ["LD C,B", 0, 1, ld_c_b],
    ["LD C,C", 0, 1, nop],
    ["LD C,D", 0, 1, ld_c_d],
    ["LD C,E", 0, 1, ld_c_e],
    ["LD C,H", 0, 1, ld_c_h],
    ["LD C,L", 0, 1, ld_c_l],
    ["LD C,(HL)", 0, 2, ld_c_hlp],
    ["LD C,A", 0, 1, ld_c_a],
    // 0x50
    ["LD D,B", 0, 1, ld_d_b],
    ["LD D,C", 0, 1, ld_d_c],
    ["LD D,D", 0, 1, nop],
    ["LD D,E", 0, 1, ld_d_e],
    ["LD D,H", 0, 1, ld_d_h],
    ["LD D,L", 0, 1, ld_d_l],
    ["LD D,(HL)", 0, 2, ld_d_hlp],
    ["LD D,A", 0, 1, ld_d_a],
    ["LD E,B", 0, 1, ld_e_b],
    ["LD E,C", 0, 1, ld_e_c],
    ["LD E,D", 0, 1, ld_e_d],
    ["LD E,E", 0, 1, nop],
    ["LD E,H", 0, 1, ld_e_h],
    ["LD E,L", 0, 1, ld_e_l],
    ["LD E,(HL)", 0, 2, ld_e_hlp],
    ["LD E,A", 0, 1, ld_e_a],
    // 0x60
    ["LD H,B", 0, 1, ld_h_b],
    ["LD H,C", 0, 1, ld_h_c],
    ["LD H,D", 0, 1, ld_h_d],
    ["LD H,E", 0, 1, ld_h_e],
    ["LD H,H", 0, 1, nop],
    ["LD H,L", 0, 1, ld_h_l],
    ["LD H,(HL)", 0, 2, ld_h_hlp],
    ["LD H,A", 0, 1, ld_h_a],
    ["LD L,B", 0, 1, ld_l_b],
    ["LD L,C", 0, 1, ld_l_c],
    ["LD L,D", 0, 1, ld_l_d],
    ["LD L,E", 0, 1, ld_l_e],
    ["LD L,H", 0, 1, ld_l_h],
    ["LD L,L", 0, 1, nop],
    ["LD L,(HL)", 0, 2, ld_l_hlp],
    ["LD L,A", 0, 1, ld_l_a],
    // 0x70
    ["LD (HL),B", 0, 2, ld_hlp_b],
    ["LD (HL),C", 0, 2, ld_hlp_c],
    ["LD (HL),D", 0, 2, ld_hlp_d],
    ["LD (HL),E", 0, 2, ld_hlp_e],
    ["LD (HL),H", 0, 2, ld_hlp_h],
    ["LD (HL),L", 0, 2, ld_hlp_l],
    ["HALT", 0, 1, halt],
    ["LD (HL),A", 0, 2, ld_hlp_a],
    ["LD A,B", 0, 1, ld_a_b],
    ["LD A,C", 0, 1, ld_a_c],
    ["LD A,D", 0, 1, ld_a_d],
    ["LD A,E", 0, 1, ld_a_e],
    ["LD A,H", 0, 1, ld_a_h],
    ["LD A,L", 0, 1, ld_a_l],
    ["LD A,(HL)", 0, 2, ld_a_hlp],
    ["LD A,A", 0, 1, nop],
    // 0x80
    ["ADD A,B", 0, 1, add_a_b],
    ["ADD A,C", 0, 1, add_a_c],
    ["ADD A,D", 0, 1, add_a_d],
    ["ADD A,E", 0, 1, add_a_e],
    ["ADD A,H", 0, 1, add_a_h],
    ["ADD A,L", 0, 1, add_a_l],
    ["ADD A,(HL)", 0, 2, add_a_hlp],
    ["ADD A", 0, 1, add_a_a],
    ["ADC B", 0, 1, adc_b],
    ["ADC C", 0, 1, adc_c],
    ["ADC D", 0, 1, adc_d],
    ["ADC E", 0, 1, adc_e],
    ["ADC H", 0, 1, adc_h],
    ["ADC L", 0, 1, adc_l],
    ["ADC (HL)", 0, 2, adc_hlp],
    ["ADC A", 0, 1, adc_a],
    // 0x90
    ["SUB B", 0, 1, sub_b],
    ["SUB C", 0, 1, sub_c],
    ["SUB D", 0, 1, sub_d],
    ["SUB E", 0, 1, sub_e],
    ["SUB H", 0, 1, sub_h],
    ["SUB L", 0, 1, sub_l],
    ["SUB (HL)", 0, 2, sub_hlp],
    ["SUB A", 0, 1, sub_a],
    ["SBC B", 0, 1, sbc_b],
    ["SBC C", 0, 1, sbc_c],
    ["SBC D", 0, 1, sbc_d],
    ["SBC E", 0, 1, sbc_e],
    ["SBC H", 0, 1, sbc_h],
    ["SBC L", 0, 1, sbc_l],
    ["SBC (HL)", 0, 2, sbc_hlp],
    ["SBC A", 0, 1, sbc_a],
    // 0xa0
    ["AND B", 0, 1, and_b],
    ["AND C", 0, 1, and_c],
    ["AND D", 0, 1, and_d],
    ["AND E", 0, 1, and_e],
    ["AND H", 0, 1, and_h],
    ["AND L", 0, 1, and_l],
    ["AND (HL)", 0, 2, and_hlp],
    ["AND A", 0, 1, and_a],
    ["XOR B", 0, 1, xor_b],
    ["XOR C", 0, 1, xor_c],
    ["XOR D", 0, 1, xor_d],
    ["XOR E", 0, 1, xor_e],
    ["XOR H", 0, 1, xor_h],
    ["XOR L", 0, 1, xor_l],
    ["XOR (HL)", 0, 2, xor_hlp],
    ["XOR A", 0, 1, xor_a],
    // 0xb0
    ["OR B", 0, 1, or_b],
    ["OR C", 0, 1, or_c],
    ["OR D", 0, 1, or_d],
    ["OR E", 0, 1, or_e],
    ["OR H", 0, 1, or_h],
    ["OR L", 0, 1, or_l],
    ["OR (HL)", 0, 2, or_hlp],
    ["OR A", 0, 1, or_a],
    ["CP B", 0, 1, cp_b],
    ["CP C", 0, 1, cp_c],
    ["CP D", 0, 1, cp_d],
    ["CP E", 0, 1, cp_e],
    ["CP H", 0, 1, cp_h],
    ["CP L", 0, 1, cp_l],
    ["CP (HL)", 0, 2, cp_hlp],
    ["CP A", 0, 1, cp_a],
    // 0xc0
    ["RET NZ", 0, 1, ret_nz],
    ["POP BC", 0, 3, pop_bc],
    ["JP NZ,nn", 2, 3, jp_nz_nn],
    ["JP nn", 2, 3, jp_nn],
    ["CALL NZ,nn", 2, 3, call_nz_nn],
    ["PUSH BC", 0, 8, push_bc],
    ["ADD A,n", 1, 2, add_a_n],
    ["RST 0x00", 0, 3, rst_0],
    ["RET Z", 0, 3, ret_z],
    ["RET", 0, 3, ret],
    ["JP Z,nn", 2, 3, jp_z_nn],
    ["CB n", 1, 0, cb_n],
    ["CALL Z,nn", 2, 3, call_z_nn],
    ["CALL nn", 2, 3, call_nn],
    ["ADC n", 1, 2, adc_n],
    ["RST 0x08", 0, 3, rst_08],
    // 0xd0
    ["RET NC", 0, 3, ret_nc],
    ["POP DE", 0, 3, pop_de],
    ["JP NC,nn", 2, 3, jp_nc_nn],
    ["UNKNOWN", 0, 0, unknown],
    ["CALL NC,nn", 2, 3, call_nc_nn],
    ["PUSH DE", 0, 8, push_de],
    ["SUB n", 1, 2, sub_n],
    ["RST 0x10", 0, 3, rst_10],
    ["RET C", 0, 3, ret_c],
    ["RETI", 0, 3, reti],
    ["JP C,nn", 2, 3, jp_c_nn],
    ["UNKNOWN", 0, 0, unknown],
    ["CALL C,nn", 2, 3, call_c_nn],
    ["UNKNOWN", 0, 0, unknown],
    ["SBC n", 1, 1, sbc_n],
    ["RST 0x18", 0, 3, rst_18],
    // 0xe0
    ["LD (0xFF00 + n),  A", 1, 3, ld_ff_n_ap],
    ["POP HL", 0, 3, pop_hl],
    ["LD (0xFF00 + C),  A", 0, 2, ld_ff_c_a],
    ["UNKNOWN", 0, 0, unknown],
    ["UNKNOWN", 0, 0, unknown],
    ["PUSH HL", 0, 3, push_hl],
    ["AND n", 1, 1, and_n],
    ["RST 0x20", 0, 3, rst_20],
    ["ADD SP, n", 1, 3, add_sp_n],
    ["JP HL", 0, 3, jp_hl],
    ["LD (nn),  A", 2, 3, ld_nnp_a],
    ["UNKNOWN", 0, 0, unknown],
    ["UNKNOWN", 0, 0, unknown],
    ["UNKNOWN", 0, 0, unknown],
    ["XOR n", 1, 2, xor_n],
    ["RST 0x28", 0, 3, rst_28],
    // 0xf0
    ["LD A,(0xFF00+n)", 1, 3, ld_ff_ap_n],
    ["POP AF", 0, 3, pop_af],
    ["LD A,(0xFF00+C)", 0, 2, ld_a_ff_c],
    ["DI", 0, 2, di],
    ["UNKNOWN", 0, 0, unknown],
    ["PUSH AF", 0, 3, push_af],
    ["OR n", 1, 2, or_n],
    ["RST 0x30", 0, 3, rst_30],
    ["LD HL,SP+n", 1, 3, ld_hl_sp_n],
    ["LD SP,HL", 0, 3, ld_sp_hl],
    ["LD A,(nn)", 2, 3, ld_a_nnp],
    ["EI", 0, 2, ei],
    ["UNKNOWN", 0, 0, unknown],
    ["UNKNOWN", 0, 0, unknown],
    ["CP n", 1, 2, cp_n],
    ["RST 0x38", 0, 3, rst_38]
];

export function unknown(cpu: CPU, mmu: MMU, operand: number) {
    throw new Error(`Unknown instruction at address ${formatHex(mmu.registers.pc - 1)}`);
}

export function nop(cpu: CPU, mmu: MMU, operand: number) { };

export function ld_bc_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.bc = operand;
};

export function ld_bcp_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.bc, mmu.registers.a);
};

export function inc_bc(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.bc++;
};

export function inc_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = inc(mmu, mmu.registers.b);
};

export function dec_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = dec(mmu, mmu.registers.b);
};

export function ld_b_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = operand;
};

export function rlca(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = rotate_left_carry(mmu, mmu.registers.a, false);
};

export function ld_nnp_sp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWord(operand, mmu.registers.sp);
};

export function add_hl_bc(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = add_word(mmu, mmu.registers.hl, mmu.registers.bc);
};

export function ld_a_bcp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(mmu.registers.bc);
};

export function dec_bc(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.bc--;
};

export function inc_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = inc(mmu, mmu.registers.c);
};

export function dec_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = dec(mmu, mmu.registers.c);
};

export function ld_c_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = operand;
};

export function rrca(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = rotate_right_carry(mmu, mmu.registers.a, false);
};

export function stop(cpu: CPU, mmu: MMU, operand: number) {
    mmu.running = false;
    // jsGB is doing something like this, not sure why
    // mmu.registers.b--;
    // if (mmu.registers.b) {
    //     mmu.registers.pc += signed(operand);
    //     mmu.registers.t++;
    // }
};

export function ld_de_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.de = operand;
};

export function ld_dep_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.de, mmu.registers.a);
};

export function inc_de(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.de++;
};

export function inc_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = inc(mmu, mmu.registers.d);
};

export function dec_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = dec(mmu, mmu.registers.d);
};

export function ld_d_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = operand;
};

export function rla(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = rotate_left(mmu, mmu.registers.a, false);
};

export function jr_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.pc += signed(operand);
};

export function add_hl_de(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = add_word(mmu, mmu.registers.hl, mmu.registers.de);
};

export function ld_a_dep(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(mmu.registers.de);
};

export function dec_de(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.de--;
};

export function inc_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = inc(mmu, mmu.registers.e);
};

export function dec_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = dec(mmu, mmu.registers.e);
};

export function ld_e_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = operand;
};

export function rra(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = rotate_right_carry(mmu, mmu.registers.a, false);
};

export function jr_nz_n(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc += signed(operand);
        mmu.registers.t++;
    }
};

export function ld_hl_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = operand;
};

export function ldi_hlp_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.a);
    mmu.registers.hl++;
};

export function inc_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl++;
};

export function inc_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = inc(mmu, mmu.registers.h);
};

export function dec_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = dec(mmu, mmu.registers.h);
};

export function ld_h_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = operand;
};

export function daa(cpu: CPU, mmu: MMU, operand: number) {
    let s = mmu.registers.a;
    if (mmu.flagIsSet(FLAG_NEGATIVE)) {
        if (mmu.flagIsSet(FLAG_HALFCARRY)) {
            s -= 6;
        }
        if (mmu.flagIsSet(FLAG_CARRY)) {
            s -= 96;
        }
    } else {
        if (mmu.flagIsSet(FLAG_HALFCARRY) || (s & 0xF) > 9) {
            s += 6;
        }
        if (mmu.flagIsSet(FLAG_CARRY) || s > 0x9F) {
            s += 96;
        }
    }
    mmu.registers.a = s;
    mmu.clearFlag(FLAG_HALFCARRY);
    if (mmu.registers.a) {
        mmu.clearFlag(FLAG_ZERO);
    } else {
        mmu.setFlag(FLAG_ZERO);
    }
    if (s >= 0x100) {
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlag(FLAG_CARRY);
    }
};

export function jr_z_n(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc += signed(operand);
        mmu.registers.t++;
    }
};

export function add_hl_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = add_word(mmu, mmu.registers.hl, mmu.registers.hl);
};

export function ldi_a_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(mmu.registers.hl);
    mmu.registers.hl++;
};

export function dec_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl--;
};

export function inc_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = inc(mmu, mmu.registers.l);
};

export function dec_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = dec(mmu, mmu.registers.l);
};

export function ld_l_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = operand;
};

export function cpl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.clearFlags();
    mmu.registers.a ^= 255;
    if (!mmu.registers.a) {
        mmu.setFlag(FLAG_ZERO);
    }
};

export function jr_nc_n(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc += signed(operand);
        mmu.registers.t++;
    }
};

export function ld_sp_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.sp = operand;
};

export function ldd_hlp_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.a);
    mmu.registers.hl--;
};

export function inc_sp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.sp++;
};

export function inc_hlp(cpu: CPU, mmu: MMU, operand: number) {
    const value = mmu.readByte(mmu.registers.hl) + 1;
    mmu.writeByte(mmu.registers.hl, value);
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    if (value === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((value & 0x0F) === 0) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
};

export function dec_hlp(cpu: CPU, mmu: MMU, operand: number) {
    const value = mmu.readByte(mmu.registers.hl) - 1;
    mmu.writeByte(mmu.registers.hl, value);
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    mmu.setFlag(FLAG_NEGATIVE);
    if (value === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((value & 0x0F) === 0) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
};

export function ld_hlp_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, operand);
};

export function scf(cpu: CPU, mmu: MMU, operand: number) {
    mmu.setFlag(FLAG_CARRY);
};

export function jr_c_n(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc += signed(operand);
        mmu.registers.t++;
    }
};

export function add_hl_sp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = add_word(mmu, mmu.registers.hl, mmu.registers.sp);
};

export function ldd_a_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(mmu.registers.hl);
    mmu.registers.hl--;
};

export function dec_sp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.sp--;
};

export function inc_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = inc(mmu, mmu.registers.a);
};

export function dec_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = dec(mmu, mmu.registers.a);
};

export function ld_a_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = operand;
};

export function ccf(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.clearFlag(FLAG_CARRY);
    } else {
        mmu.setFlag(FLAG_CARRY);
    }
};

export function ld_b_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.c;
};

export function ld_b_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.d;
};

export function ld_b_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.e;
};

export function ld_b_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.h;
};

export function ld_b_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.l;
};

export function ld_b_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.readByte(mmu.registers.hl);
};

export function ld_b_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.b = mmu.registers.a;
};

export function ld_c_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.b;
};

export function ld_c_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.d;
};

export function ld_c_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.e;
};

export function ld_c_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.h;
};

export function ld_c_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.l;
};

export function ld_c_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.readByte(mmu.registers.hl);
};

export function ld_c_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.c = mmu.registers.a;
};

export function ld_d_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.b;
};

export function ld_d_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.c;
};

export function ld_d_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.e;
};

export function ld_d_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.h;
};

export function ld_d_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.l;
};

export function ld_d_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.readByte(mmu.registers.hl);
};

export function ld_d_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.d = mmu.registers.a;
};

export function ld_e_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.b;
};

export function ld_e_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.c;
};

export function ld_e_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.d;
};

export function ld_e_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.h;
};

export function ld_e_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.l;
};

export function ld_e_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.readByte(mmu.registers.hl);
};

export function ld_e_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.e = mmu.registers.a;
};

export function ld_h_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.b;
};

export function ld_h_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.c;
};

export function ld_h_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.d;
};

export function ld_h_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.e;
};

export function ld_h_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.l;
};

export function ld_h_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.readByte(mmu.registers.hl);
};

export function ld_h_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.h = mmu.registers.a;
};

export function ld_l_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.b;
};

export function ld_l_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.c;
};

export function ld_l_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.d;
};

export function ld_l_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.e;
};

export function ld_l_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.h;
};

export function ld_l_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.readByte(mmu.registers.hl);
};

export function ld_l_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.l = mmu.registers.a;
};

export function ld_hlp_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.b);
};

export function ld_hlp_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.c);
};

export function ld_hlp_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.d);
};

export function ld_hlp_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.e);
};

export function ld_hlp_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.h);
};

export function ld_hlp_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.l);
};

export function halt(cpu: CPU, mmu: MMU, operand: number) {
    cpu.halted = true;
};

export function ld_hlp_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(mmu.registers.hl, mmu.registers.a);
};

export function ld_a_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.b;
};

export function ld_a_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.c;
};

export function ld_a_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.d;
};

export function ld_a_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.e;
};

export function ld_a_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.h;
};

export function ld_a_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.registers.l;
};

export function ld_a_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(mmu.registers.hl);
};

export function add_a_b(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.b);
};

export function add_a_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.c);
};

export function add_a_d(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.d);
};

export function add_a_e(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.e);
};

export function add_a_h(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.h);
};

export function add_a_l(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.l);
};

export function add_a_hlp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.readByte(mmu.registers.hl));
};

export function add_a_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, mmu.registers.a);
};

export function adc_b(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.b);
};

export function adc_c(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.c);
};

export function adc_d(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.d);
};

export function adc_e(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.e);
};

export function adc_h(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.h);
};

export function adc_l(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.l);
};

export function adc_hlp(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.readByte(mmu.registers.hl));
};

export function adc_a(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, mmu.registers.a);
};

export function sub_b(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.b);
};

export function sub_c(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.c);
};

export function sub_d(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.d);
};

export function sub_e(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.e);
};

export function sub_h(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.h);
};

export function sub_l(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.l);
};

export function sub_hlp(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.readByte(mmu.registers.hl));
};

export function sub_a(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, mmu.registers.a);
};

export function sbc_b(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.b);
};

export function sbc_c(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.c);
};

export function sbc_d(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.d);
};

export function sbc_e(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.e);
};

export function sbc_h(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.h);
};

export function sbc_l(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.l);
};

export function sbc_hlp(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.readByte(mmu.registers.hl));
};

export function sbc_a(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, mmu.registers.a);
};

export function and_b(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.b);
};

export function and_c(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.c);
};

export function and_d(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.d);
};

export function and_e(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.e);
};

export function and_h(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.h);
};

export function and_l(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.l);
};

export function and_hlp(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.readByte(mmu.registers.hl));
};

export function and_a(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, mmu.registers.a);
};

export function xor_b(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.b);
};

export function xor_c(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.c);
};

export function xor_d(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.d);
};

export function xor_e(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.e);
};

export function xor_h(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.h);
};

export function xor_l(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.l);
};

export function xor_hlp(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.readByte(mmu.registers.hl));
};

export function xor_a(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, mmu.registers.a);
};

export function or_b(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.b);
};

export function or_c(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.c);
};

export function or_d(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.d);
};

export function or_e(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.e);
};

export function or_h(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.h);
};

export function or_l(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.l);
};

export function or_hlp(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.readByte(mmu.registers.hl));
};

export function or_a(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, mmu.registers.a);
};

export function cp_b(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.b);
};

export function cp_c(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.c);
};

export function cp_d(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.d);
};

export function cp_e(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.e);
};

export function cp_h(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.h);
};

export function cp_l(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.l);
};

export function cp_hlp(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.readByte(mmu.registers.hl));
};

export function cp_a(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, mmu.registers.a);
};

export function ret_nz(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc = mmu.readWordFromStack();
        mmu.registers.t += 2;
    }
};

export function pop_bc(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.bc = mmu.readWordFromStack();
};

export function jp_nz_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function jp_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.pc = operand;
};

export function call_nz_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_ZERO)) {
        mmu.writeWordToStack(mmu.registers.pc);
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function push_bc(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWordToStack(mmu.registers.bc);
};

export function add_a_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = add_byte(mmu, mmu.registers.a, operand);
};

export function rst_0(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0000;
};

export function ret_z(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc = mmu.readWordFromStack();
        mmu.registers.t += 2;
    }
};

export function ret(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.pc = mmu.readWordFromStack();
};

export function jp_z_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_ZERO)) {
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function cb_n(cpu: CPU, mmu: MMU, operand: number) {
    if (extendedInstructions[operand]) {
        const [
            name,
            cycles,
            operation
        ] = extendedInstructions[operand];
        // TO DO: Some logging here
        operation(mmu);
        mmu.registers.t += cycles;
    } else {
        throw new Error(`Unknown extended instruction ${operand.toString(16)}`);
    }
}

export function call_z_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_ZERO)) {
        mmu.writeWordToStack(mmu.registers.pc);
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function call_nn(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = operand;
};

export function adc_n(cpu: CPU, mmu: MMU, operand: number) {
    add_carry_to_a(mmu, operand);
};

export function rst_08(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0008;
};

export function ret_nc(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc = mmu.readWordFromStack();
        mmu.registers.t += 2;
    }
};

export function pop_de(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.de = mmu.readWordFromStack();
};

export function jp_nc_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function call_nc_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (!mmu.flagIsSet(FLAG_CARRY)) {
        mmu.writeWordToStack(mmu.registers.pc);
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function push_de(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWordToStack(mmu.registers.de);
};

export function sub_n(cpu: CPU, mmu: MMU, operand: number) {
    sub(mmu, operand);
};

export function rst_10(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0010;
};

export function ret_c(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc = mmu.readWordFromStack();
        mmu.registers.t += 2;
    }
};

export function reti(cpu: CPU, mmu: MMU, operand: number) {
    // returnFromInterrupt(mmu, );
    mmu.registers.restore();
    throw new Error("Unimplemented");
};

export function jp_c_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function call_c_nn(cpu: CPU, mmu: MMU, operand: number) {
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.writeWordToStack(mmu.registers.pc);
        mmu.registers.pc = operand;
        mmu.registers.t += 2;
    }
};

export function sbc_n(cpu: CPU, mmu: MMU, operand: number) {
    sub_carry_from_a(mmu, operand);
};

export function rst_18(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0018;
};

export function ld_ff_n_ap(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(0xff00 + operand, mmu.registers.a);
};

export function pop_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = mmu.readWordFromStack();
};

export function ld_ff_c_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(0xff00 + mmu.registers.c, mmu.registers.a);
};

export function push_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWordToStack(mmu.registers.hl);
};

export function and_n(cpu: CPU, mmu: MMU, operand: number) {
    and(mmu, operand);
};

export function rst_20(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0020;
};

export function add_sp_n(cpu: CPU, mmu: MMU, operand: number) {
    const result = mmu.registers.sp + operand;
    mmu.registers.sp += signed(operand);
};

export function jp_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.pc = mmu.registers.hl;
};

export function ld_nnp_a(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeByte(operand, mmu.registers.a);
};

export function xor_n(cpu: CPU, mmu: MMU, operand: number) {
    xor(mmu, operand);
};

export function rst_28(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0028;
};

export function ld_ff_ap_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(0xff00 + operand);
};

export function pop_af(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.af = mmu.readWordFromStack();
};

export function ld_a_ff_c(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(0xff00 + mmu.registers.c);
};

export function di(cpu: CPU, mmu: MMU, operand: number) {
    // interrupt.master = 0;
    throw new Error("Unimplemented");
};

export function push_af(cpu: CPU, mmu: MMU, operand: number) {
    mmu.writeWordToStack(mmu.registers.af);
};

export function or_n(cpu: CPU, mmu: MMU, operand: number) {
    or(mmu, operand);
};

export function rst_30(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0030;
};

export function ld_hl_sp_n(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.hl = mmu.registers.sp + signed(operand);
};

export function ld_sp_hl(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.sp = mmu.registers.hl;
};

export function ld_a_nnp(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.a = mmu.readByte(operand);
};

export function ei(cpu: CPU, mmu: MMU, operand: number) {
    // interrupt.master = 1;
    throw new Error("Unimplemented");
};

export function cp_n(cpu: CPU, mmu: MMU, operand: number) {
    compare_a(mmu, operand);
};

export function rst_38(cpu: CPU, mmu: MMU, operand: number) {
    mmu.registers.save();
    mmu.writeWordToStack(mmu.registers.pc);
    mmu.registers.pc = 0x0038;
}
