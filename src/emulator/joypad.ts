type Keys = [number, number];

export class Joypad {
    private keys: Keys;
    private row: number;

    constructor() {
        this.keys = [0, 0];
        this.row = 0;
    }

    read(): number {
        if (this.row === 0x10) {
            return this.keys[0];
        }
        if (this.row === 0x20) {
            return this.keys[1];
        }
        return 0;
    }

    write(value: number) {
        this.row = value & 0x30;
    }

    keyDown(event: KeyboardEvent) {
        switch (event.keyCode) {
            case 39:
                this.keys[1] &= 0xe;
                break;
            case 37:
                this.keys[1] &= 0xd;
                break;
            case 38:
                this.keys[1] &= 0xb;
                break;
            case 40:
                this.keys[1] &= 0x7;
                break;
            case 90:
                this.keys[0] &= 0xe;
                break;
            case 88:
                this.keys[0] &= 0xd;
                break;
            case 32:
                this.keys[0] &= 0xb;
                break;
            case 13:
                this.keys[0] &= 0x7;
                break;
        }
    }

    keyUp(event: KeyboardEvent) {
        switch (event.keyCode) {
            case 39:
                this.keys[1] |= 0x1;
                break;
            case 37:
                this.keys[1] |= 0x2;
                break;
            case 38:
                this.keys[1] |= 0x4;
                break;
            case 40:
                this.keys[1] |= 0x8;
                break;
            case 90:
                this.keys[0] |= 0x1;
                break;
            case 88:
                this.keys[0] |= 0x2;
                break;
            case 32:
                this.keys[0] |= 0x5;
                break;
            case 13:
                this.keys[0] |= 0x8;
                break;
        }
    }
}