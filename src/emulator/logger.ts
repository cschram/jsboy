export enum LogLevel {
    ERROR,
    WARN,
    INFO,
    DEBUG
}

export type LogEntry = [string, any];

export class Logger {
    private prefix: string;
    private level: LogLevel;
    private historyDepth: number;
    private history: Array<LogEntry>;

    constructor(prefix: string, level = LogLevel.INFO, historyDepth = 1000) {
        this.prefix = prefix;
        this.level = level;
        this.historyDepth = historyDepth;
        this.history = [];
    }

    public getHistory(): Array<LogEntry> {
        return this.history;
    }

    public clearHistory() {
        this.history = [];
    }

    public error(...args: any[]) {
        args.forEach(arg => {
            this.pushEntry("error", arg);
            console.error(`[${this.prefix}:error] ${arg.toString()}`);
        });
    }

    public warn(...args: any[]) {
        args.forEach(arg => {
            this.pushEntry("warn", arg);
            if (this.level >= LogLevel.WARN) {
                console.warn(`[${this.prefix}:warn] ${arg.toString()}`);
            }
        });
    }

    public info(...args: any[]) {
        args.forEach(arg => {
            this.pushEntry("info", arg);
            if (this.level >= LogLevel.INFO) {
                console.log(`[${this.prefix}:info] ${arg.toString()}`);
            }
        });
    }

    public debug(...args: any[]) {
        args.forEach(arg => {
            this.pushEntry("debug", arg);
            if (this.level >= LogLevel.DEBUG) {
                console.log(`[${this.prefix}:debug] ${arg.toString()}`);
            }
        });
    }

    private pushEntry(type: string, entry: any) {
        this.history.push([type, entry]);
        if (this.history.length > this.historyDepth) {
            this.history.shift();
        }
    }
}