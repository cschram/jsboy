import { JSBoy } from "./index";
import { Joypad } from "./joypad";
import { Logger, LogLevel, LogEntry } from "./logger";
import { GPU, BASE_PALETTE } from "./gpu";
import { Registers } from "./registers";
import { formatHex } from "./util";

const BIOS = [
    0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E,
    0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0,
    0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A, 0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B,
    0xFE, 0x34, 0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06, 0x08, 0x1A, 0x13, 0x22, 0x23, 0x05, 0x20, 0xF9,
    0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21, 0x2F, 0x99, 0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
    0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64, 0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04,
    0x1E, 0x02, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2,
    0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62, 0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06,
    0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xF2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15, 0x20, 0xD2, 0x05, 0x20,
    0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F, 0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
    0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
    0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
    0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
    0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0x3c, 0x42, 0xB9, 0xA5, 0xB9, 0xA5, 0x42, 0x4C,
    0x21, 0x04, 0x01, 0x11, 0xA8, 0x00, 0x1A, 0x13, 0xBE, 0x20, 0xFE, 0x23, 0x7D, 0xFE, 0x34, 0x20,
    0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86, 0x20, 0xFE, 0x3E, 0x01, 0xE0, 0x50
];

export const FLAG_ZERO = 0x80;
export const FLAG_NEGATIVE = 0x40;
export const FLAG_HALFCARRY = 0x20;
export const FLAG_CARRY = 0x10;

export enum ROMType {
    ROM_ONLY,
    ROM_MBC1,
    ROM_MBC1_RAM,
    ROM_MBC1_RAM_BATT,
    ROM_MBC2,
    ROM_MBC2_BATTERY,
    ROM_RAM,
    ROM_RAM_BATTERY,
    ROM_MMM01,
    ROM_MMM01_SRAM,
    ROM_MMM01_SRAM_BATT,
    ROM_MBC3_RAM,
    ROM_MBC3_RAM_BATT,
    ROM_MBC5,
    ROM_MBC5_RAM,
    ROM_MBC5_RAM_BATT,
    ROM_MBC5_RUMBLE,
    ROM_MBC5_RUMBLE_SRAM,
    ROM_MBC5_RUMBLE_SRAM_BATT,
    POCKET_CAMERA,
    BANDAI_TAMA5,
    HUDSON_HUC3
}

export class MMU {
    public running: boolean;
    public debug: boolean;
    public registers: Registers;

    private emulator: JSBoy;
    private logger: Logger;
    private inBIOS: boolean;
    private rom: Uint8Array;
    private vram: Uint8Array;
    private eram: Uint8Array;
    private wram: Uint8Array;
    private oam: Uint8Array;
    private io: Uint8Array;
    private zram: Uint8Array;
    private romType: ROMType;
    private romOffset: number;
    private ramOffset: number;

    constructor(emulator: JSBoy) {
        this.emulator = emulator;
        const { logLevel, logHistoryDepth } = emulator.getOptions();
        this.logger = new Logger("MMU", logLevel, logHistoryDepth);
        this.running = false;
        this.debug = false;
        this.reset();
    }

    public getLogHistory(): Array<LogEntry> {
        return this.logger.getHistory();
    }

    public clearLogHistory() {
        this.logger.clearHistory();
    }

    public printRegisters() {
        this.logger.info(`Register A: ${formatHex(this.registers.a)}`);
        this.logger.info(`Register F: ${formatHex(this.registers.f)}`);
        this.logger.info(`Register AF: ${formatHex(this.registers.af)}`);
        this.logger.info(`Register B: ${formatHex(this.registers.b)}`);
        this.logger.info(`Register C: ${formatHex(this.registers.c)}`);
        this.logger.info(`Register BC: ${formatHex(this.registers.bc)}`);
        this.logger.info(`Register D: ${formatHex(this.registers.d)}`);
        this.logger.info(`Register E: ${formatHex(this.registers.e)}`);
        this.logger.info(`Register DE: ${formatHex(this.registers.de)}`);
        this.logger.info(`Register H: ${formatHex(this.registers.h)}`);
        this.logger.info(`Register L: ${formatHex(this.registers.l)}`);
        this.logger.info(`Register HL: ${formatHex(this.registers.hl)}`);
        this.logger.info(`Register PC: ${formatHex(this.registers.pc)}`);
        this.logger.info(`Register SP: ${formatHex(this.registers.sp)}`);
    }

    public reset() {
        this.clearLogHistory();
        this.inBIOS = true;
        this.rom = new Uint8Array(0x8000);
        this.vram = new Uint8Array(0x2000);
        this.eram = new Uint8Array(0x2000);
        this.wram = new Uint8Array(0x2000);
        this.oam = new Uint8Array(0x100);
        this.io = new Uint8Array(0x100);
        this.zram = new Uint8Array(0x80);
        this.registers = new Registers();
        this.romType = 0;
        this.romOffset = 0x4000;
        this.ramOffset = 0;
    }

    public loadROM(rom: Uint8Array) {
        for (let i = 0, len = this.rom.length; i < len; i++) {
            this.rom[i] = rom[i];
        }
        this.romType = this.rom[147];
    }

    public getROMType() {
        return this.romType;
    }

    public readByte(address: number): number {
        if (address <= 0x0100) {
            // BIOS and ROM Bank 0
            if (this.inBIOS) {
                if (address === 0x0100) {
                    this.logger.info("Leaving BIOS");
                    this.printRegisters();
                    this.inBIOS = false;
                } else {
                    return BIOS[address];
                }
            }
            return this.rom[address];
        }
        if (address < 0x4000) {
            // ROM Bank 0
            return this.rom[address];
        } else if (address < 0x8000) {
            // ROM Bank 1
            return this.rom[this.romOffset + address - 0x4000];
        } else if (address < 0xA000) {
            // Video RAM
            return this.vram[address - 0x8000];
        } else if (address < 0xC000) {
            // External RAM
            return this.eram[this.ramOffset + address - 0xA000];
        } else if (address < 0xE000) {
            // Working RAM
            return this.wram[address - 0xC000];
        } else if (address < 0xFE00) {
            // Working RAM Shadow
            return this.wram[address - 0xE000];
        } else if (address < 0xFEFF) {
            // Object Attribute Memory
            return this.oam[address - 0xFE00];
        } else if (address < 0xFF80) {
            // IO and GPU State
            if (address === 0xFF00) {
                // Input
                return this.joypad.read();
            } else if (address === 0xFF04) {
                // DIV Timer
            } else if (address === 0xFF05) {
                // TIMA Timer
            } else if (address === 0xFF06) {
                // TMA Timer
            } else if (address === 0xFF07) {
                // TAC Timer
            } else if (address === 0xFF0F) {
                // Interrupt flags
            } else if (address === 0xFF40) {
                return this.gpu.getFlags();
            } else if (address === 0xFF42) {
                return this.gpu.getScrollY();
            } else if (address === 0xFF43) {
                return this.gpu.getScrollX();
            } else if (address === 0xFF44) {
                return this.gpu.getScanline();
            } else {
                return this.io[address - 0xFEFF];
            }
        } else if (address < 0xFFFF) {
            return this.zram[address - 0xFF80];
        } else if (address === 0xFFFF) {
            // Interrupt enable
        }
        return 0;
    }

    public readWord(address: number): number {
        return this.readByte(address) + (this.readByte(address + 1) << 8);
    }

    public readByteFromStack(): number {
        const value = this.readByte(this.registers.sp);
        this.registers.sp++;
        return value;
    }

    public readWordFromStack(): number {
        const value = this.readWord(this.registers.sp);
        this.registers.sp += 2;
        return value;
    }

    public writeByte(address: number, value: number) {
        if (address < 0x2000) {
            // Turn External RAM off/on
        } else if (address < 0x4000) {
            // ROM Bank switching
        } else if (address < 0x6000) {
            // RAM Bank switching
        } else if (address < 0x8000) {
            // MBC Mode  
        } else if (address < 0xA000) {
            this.vram[address - 0x8000] = value;
            this.gpu.updateVRAM(address - 0x8000, this.vram);
        } else if (address < 0xC000) {
            this.vram[address - 0xA000] = value;
        } else if (address < 0xE000) {
            this.wram[address - 0xC000] = value;
        } else if (address < 0xFE00) {
            this.wram[address - 0xE000] = value;
        } else if (address < 0xFF00) {
            this.oam[address - 0xFE00] = value;
            this.gpu.updateOAM(address - 0xFE00, value);
        } else if (address < 0xFF80) {
            // IO and GPU State
            if (address === 0xFF00) {
                // Input
                this.joypad.write(value);
            } else if (address === 0xFF04) {
                // DIV Timer
            } else if (address === 0xFF05) {
                // TIMA Timer
            } else if (address === 0xFF06) {
                // TMA Timer
            } else if (address === 0xFF07) {
                // TAC Timer
            } else if (address === 0xFF0F) {
                // Interrupt flags
            } else if (address === 0xFF40) {
                this.gpu.setFlags(value);
            } else if (address === 0xFF42) {
                this.gpu.setScrollY(value);
            } else if (address === 0xFF43) {
                this.gpu.setScrollX(value);
            } else if (address === 0xFF46) {
                for (let i = 0; i < 160; i++) {
                    this.writeByte(0xFE00 + i, (value << 8) + i);
                }
            } else if (address === 0xFF47) {
                this.gpu.setBGPalette(value);
            } else if (address === 0xFF48) {
                this.gpu.setSpritePalette(0, value);
            } else if (address === 0xFF49) {
                this.gpu.setSpritePalette(1, value);
            } else {
                this.io[address - 0xFEFF] = value;
            }
        } else {
            this.zram[address - 0xFF80] = value;
        }
    }

    public writeWord(address: number, value: number) {
        this.writeByte(address, value & 255);
        this.writeByte(address + 1, value >> 8);
    }

    public writeByteToStack(value: number) {
        this.registers.sp--;
        this.writeByte(this.registers.sp, value);
    }

    public writeWordToStack(value: number) {
        this.registers.sp -= 2;
        this.writeWord(this.registers.sp, value);
    }

    public flagIsSet(flag: number): boolean {
        return !!(this.registers.f & flag);
    };

    public setFlag(flag: number) {
        this.registers.f |= flag;
    };

    public clearFlags() {
        this.registers.f = 0;
    }

    public clearFlag(flag: number) {
        this.registers.f &= ~flag;
    };

    private get gpu(): GPU {
        return this.emulator.getGPU();
    }

    private get joypad(): Joypad {
        return this.emulator.getJoypad();
    }
}