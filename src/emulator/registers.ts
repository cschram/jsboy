export class Registers {
    // Clock timer
    public t: number;

    private bytes: Uint8Array;
    private saved: Uint8Array;

    constructor(buffer?: ArrayBuffer) {
        this.bytes = new Uint8Array(12);
        this.saved = new Uint8Array(8);
    }

    // A 8-bit Register
    public get a(): number {
        return this.bytes[0];
    }
    public set a(value: number) {
        this.bytes[0] = value;
    }

    // F (Flags) 8-bit Register
    public get f(): number {
        return this.bytes[1];
    }
    public set f(value: number) {
        this.bytes[1] = value;
    }

    // AF 16-bit Register (A and F combined)
    public get af(): number {
        return (this.bytes[0] << 8) + this.bytes[1];
    }
    public set af(value: number) {
        this.bytes[0] = (value >> 8) & 255;
        this.bytes[1] = value & 255;
    }

    // B 8-bit Register
    public get b(): number {
        return this.bytes[2];
    }
    public set b(value: number) {
        this.bytes[2] = value;
    }

    // C 8-bit Register
    public get c(): number {
        return this.bytes[3];
    }
    public set c(value: number) {
        this.bytes[3] = value;
    }

    // BC 16-bit Register (B and C combined)
    public get bc(): number {
        return (this.bytes[2] << 8) + this.bytes[3];
    }
    public set bc(value: number) {
        this.bytes[2] = (value >> 8) & 255;
        this.bytes[3] = value & 255;
    }

    // D 8-bit Register
    public get d(): number {
        return this.bytes[4];
    }
    public set d(value: number) {
        this.bytes[4] = value;
    }

    // E 8-bit Register
    public get e(): number {
        return this.bytes[5];
    }
    public set e(value: number) {
        this.bytes[5] = value;
    }

    // DE 16-bit Register (D and E combined)
    public get de(): number {
        return (this.bytes[4] << 8) + this.bytes[5];
    }
    public set de(value: number) {
        this.bytes[4] = (value >> 8) & 255;
        this.bytes[5] = value & 255;
    }

    // H 8-bit Register
    public get h(): number {
        return this.bytes[6];
    }
    public set h(value: number) {
        this.bytes[6] = value;
    }

    // L 8-bit Register
    public get l(): number {
        return this.bytes[7];
    }
    public set l(value: number) {
        this.bytes[7] = value;
    }

    // HL 16-bit Register (H and L combined)
    public get hl(): number {
        return (this.bytes[6] << 8) + this.bytes[7];
    }
    public set hl(value: number) {
        this.bytes[6] = (value >> 8) & 255;
        this.bytes[7] = value & 255;
    }

    // SP (Stack Pointer) 16-bit Register
    public get sp(): number {
        return (this.bytes[8] << 8) + this.bytes[9];
    }
    public set sp(value: number) {
        this.bytes[8] = (value >> 8) & 255;
        this.bytes[9] = value & 255;
    }

    // PC (Program Counter) 16-bit Register
    public get pc(): number {
        return (this.bytes[10] << 8) + this.bytes[11];
    }
    public set pc(value: number) {
        this.bytes[10] = (value >> 8) & 255;
        this.bytes[11] = value & 255;
    }

    public save() {
        for (let i = 0; i < 8; i++) {
            this.saved[i] = this.bytes[i];
        }
    }

    public restore() {
        for (let i = 0; i < 8; i++) {
            this.bytes[i] = this.saved[i];
        }
    }
}