import {
    MMU,
    FLAG_ZERO,
    FLAG_NEGATIVE,
    FLAG_HALFCARRY,
    FLAG_CARRY
} from "./mmu";

export function formatHex(value: number): string {
    if (isNaN(value)) {
        console.log(value);
        return "0x????";
    } else {
        return `0x${value.toString(16).toUpperCase()}`;
    }
}

export function signed(value: number): number {
    return value > 127 ? -((~value + 1) & 255) : value;
}

export function inc(mmu: MMU, value: number): number {
    mmu.clearFlags();
    const result = (value + 1) & 255;
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((result & 0x0F) === 0x00) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    return result;
};

export function dec(mmu: MMU, value: number): number {
    mmu.clearFlags();
    const result = (value - 1) & 255;
    if (mmu.flagIsSet(FLAG_CARRY)) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((result & 0x0F) === 0x0F) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    return result;
};

export function add_byte(mmu: MMU, a: number, b: number): number {
    const result = a + b;
    const carry = a ^ b ^ result;
    mmu.clearFlags();
    if ((result & 255) === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((carry & 0x100) !== 0) {
        mmu.setFlag(FLAG_CARRY);
    }
    if ((carry & 0x10) !== 0) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    return result & 255;
}

export function add_word(mmu: MMU, a: number, b: number): number {
    const result = a + b;
    if (mmu.flagIsSet(FLAG_ZERO)) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_ZERO);
    } else {
        mmu.clearFlags();
    }
    if (result & 0x10000) {
        mmu.setFlag(FLAG_CARRY);
    }
    if ((a ^ b ^ (result & 0xFFFF)) & 0x1000) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    return result & 65535;
}

export function add_carry_to_a(mmu: MMU, value: number) {
    const carry = mmu.flagIsSet(FLAG_CARRY) ? 1 : 0;
    const result = mmu.registers.a + value + carry;
    mmu.clearFlags();
    if ((result & 255) === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if (result > 0xFF) {
        mmu.setFlag(FLAG_CARRY);
    }
    if (((mmu.registers.a & 0x0F) + (value & 0x0F) + carry) > 0x0F) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    mmu.registers.a = result & 255;
};

export function sub_carry_from_a(mmu: MMU, value: number) {
    const carry = mmu.flagIsSet(FLAG_CARRY) ? 1 : 0;
    const result = mmu.registers.a - value - carry;
    mmu.clearFlags();
    mmu.setFlag(FLAG_NEGATIVE);
    if ((result & 255) === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if (result < 0) {
        mmu.setFlag(FLAG_CARRY);
    }
    if (((mmu.registers.a & 0x0F) - (value & 0x0F) - carry) < 0) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    mmu.registers.a = result & 255;
};

export function sub(mmu: MMU, value: number) {
    const result = mmu.registers.a - value;
    const carry = mmu.registers.a ^ value ^ result;
    mmu.clearFlags();
    mmu.setFlag(FLAG_NEGATIVE);
    if ((result & 255) === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    if ((carry & 0x100) !== 0) {
        mmu.setFlag(FLAG_CARRY);
    }
    if ((carry & 0x10) !== 0) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
    return result & 255;
};

export function and(mmu: MMU, value: number) {
    mmu.clearFlags();
    mmu.registers.a &= value;
    if (mmu.registers.a === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
};

export function or(mmu: MMU, value: number) {
    mmu.clearFlags();
    mmu.registers.a |= value;
    if (mmu.registers.a === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
};

export function xor(mmu: MMU, value: number) {
    mmu.clearFlags();
    mmu.registers.a ^= value;
    if (mmu.registers.a === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
};

export function compare_a(mmu: MMU, value: number) {
    mmu.clearFlags();
    mmu.setFlag(FLAG_NEGATIVE);
    if (mmu.registers.a < value) {
        mmu.setFlag(FLAG_CARRY);
    }
    if (mmu.registers.a === value) {
        mmu.setFlag(FLAG_ZERO);
    }
    if (((mmu.registers.a - value) & 0xF) > (mmu.registers.a & 0xF)) {
        mmu.setFlag(FLAG_HALFCARRY);
    }
};

export function rotate_left_carry(mmu: MMU, value: number, checkZero = true): number {
    mmu.clearFlags();
    let result = value << 1;
    if (value & 0x80) {
        result += 1;
        mmu.setFlag(FLAG_CARRY);
    }
    result = result & 255;
    if (checkZero && result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function rotate_right_carry(mmu: MMU, value: number, checkZero = true): number {
    mmu.clearFlags();
    let result = value >> 1;
    if (value & 1) {
        mmu.setFlag(FLAG_CARRY);
        result += 0x80;
    }
    result = result & 255;
    if (checkZero && result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function rotate_left(mmu: MMU, value: number, checkZero = true): number {
    const carry = mmu.flagIsSet(FLAG_CARRY) ? 1 : 0;
    if (value & 0x80) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    const result = ((value << 1) + carry) & 255;
    if (checkZero && result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function rotate_right(mmu: MMU, value: number, checkZero = true): number {
    const carry = mmu.flagIsSet(FLAG_CARRY) ? 0x80 : 0;
    if (value & 1) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    const result = ((value >> 1) + carry) & 255;
    if (checkZero && result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function shift_left_sign(mmu: MMU, value: number): number {
    if (value & 0x80) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    const result = (value << 1) & 255;
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function shift_right_sign(mmu: MMU, value: number): number {
    if (value & 1) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    let result = (value >> 1) & 255;
    if (value & 0x80) {
        result |= 0x80;
    }
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function swap(mmu: MMU, value: number): number {
    mmu.clearFlags();
    const result = (value & 0x0F) + ((value >> 4) & 0x0F);
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function shift_right(mmu: MMU, value: number): number {
    if (value & 1) {
        mmu.clearFlags();
        mmu.setFlag(FLAG_CARRY);
    } else {
        mmu.clearFlags();
    }
    const result = (value >> 1) & 255;
    if (result === 0) {
        mmu.setFlag(FLAG_ZERO);
    }
    return result;
}

export function test_bit(mmu: MMU, bit: number, value: number) {
    if (((value >> bit) & 1) === 0) {
        mmu.setFlag(FLAG_ZERO);
    } else {
        mmu.clearFlag(FLAG_ZERO);
    }
    mmu.setFlag(FLAG_HALFCARRY);
    mmu.clearFlag(FLAG_NEGATIVE);
}

export function set_bit(mmu: MMU, bit: number, value: number): number {
    return value | bit;
}