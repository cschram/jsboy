import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app";
import "./style.css";

ReactDOM.render(<App />, document.getElementById("main"));
// const emulator = new JSBoy(<HTMLCanvasElement>document.getElementById("screen"), {
//     logLevel: LogLevel.INFO,
//     logHistoryDepth: 5000
// });

// const debugPanel = <HTMLElement>document.getElementsByClassName("debug-panel")[0];

// const romSelect = <HTMLSelectElement>document.getElementById("rom");
// romSelect.addEventListener("change", () => {
//     startButton.setAttribute("disabled", "disabled");
//     emulator.loadROM(romSelect.value).then(() => {
//         startButton.removeAttribute("disabled");
//     });
// });

// const debugInput = <HTMLInputElement>document.getElementById("debug");
// debugInput.addEventListener("change", () => {
//     if (debugInput.checked) {
//         debugPanel.style.display = "block";
//     } else {
//         debugPanel.style.display = "none";
//     }
// });

// const startButton = <HTMLButtonElement>document.getElementById("start");
// startButton.setAttribute("disabled", "disabled");
// startButton.addEventListener("click", () => {
//     emulator.start();
// });

// const pauseButton = <HTMLButtonElement>document.getElementById("pause");
// pauseButton.addEventListener("click", () => {
//     emulator.pause();
// });

// const resetButton = <HTMLButtonElement>document.getElementById("reset");
// resetButton.addEventListener("click", () => {
//     emulator.reset();
// });

// const debugTitle = document.getElementById("debug-title");
// const debugPauseFrame = <HTMLInputElement>document.getElementById("debug-pause-frame");
// const debugRegisterA = document.getElementById("debug-register-a");
// const debugRegisterF = document.getElementById("debug-register-f");
// const debugRegisterB = document.getElementById("debug-register-b");
// const debugRegisterC = document.getElementById("debug-register-c");
// const debugRegisterD = document.getElementById("debug-register-d");
// const debugRegisterE = document.getElementById("debug-register-e");
// const debugRegisterH = document.getElementById("debug-register-h");
// const debugRegisterL = document.getElementById("debug-register-l");
// const debugRegisterSP = document.getElementById("debug-register-sp");
// const debugRegisterPC = document.getElementById("debug-register-pc");
// const debugGPUFlagBGEnabled = document.getElementById("debug-gpu-flag-bg-enabled");
// const debugGPUFlagSpritesEnabled = document.getElementById("debug-gpu-flag-sprites-enabled");
// const debugGPUFlagSpriteSize = document.getElementById("debug-gpu-flag-sprite-size");
// const debugGPUFlagBGTileMap = document.getElementById("debug-gpu-flag-bg-tile-map");
// const debugGPUFlagBGTileSet = document.getElementById("debug-gpu-flag-bg-tile-set");
// const debugGPUFlagWindowEnabled = document.getElementById("debug-gpu-flag-window-enabled");
// const debugGPUFlagWindowTileMap = document.getElementById("debug-gpu-flag-window-tile-map");
// const debugGPUFlagDisplayEnabled = document.getElementById("debug-gpu-flag-display-enabled");
// const debugTileset0 = <HTMLCanvasElement>document.getElementById("debug-tileset-0");
// const debugTileset0Ctx = debugTileset0.getContext("2d");
// debugTileset0Ctx.fillStyle = "rgb(255, 255, 255)";
// debugTileset0Ctx.fillRect(0, 0, 256, 256);
// const debugTileset1 = <HTMLCanvasElement>document.getElementById("debug-tileset-1");
// const debugTileset1Ctx = debugTileset1.getContext("2d");
// debugTileset1Ctx.fillStyle = "rgb(255, 255, 255)";
// debugTileset1Ctx.fillRect(0, 0, 256, 256);

// emulator.events.on("start", () => {
//     debugTitle.innerText = "Debug Panel (Running)";
// });

// emulator.events.on("pause", () => {
//     debugTitle.innerText = "Debug Panel (Paused)";
// });

// emulator.events.on("frame", () => {
//     const mmu = emulator.getMMU();
//     const gpu = emulator.getGPU();
//     debugRegisterA.innerText = formatHex(mmu.registers.a);
//     debugRegisterF.innerText = formatHex(mmu.registers.f);
//     debugRegisterB.innerText = formatHex(mmu.registers.b);
//     debugRegisterC.innerText = formatHex(mmu.registers.c);
//     debugRegisterD.innerText = formatHex(mmu.registers.d);
//     debugRegisterE.innerText = formatHex(mmu.registers.e);
//     debugRegisterH.innerText = formatHex(mmu.registers.h);
//     debugRegisterL.innerText = formatHex(mmu.registers.l);
//     debugRegisterSP.innerText = formatHex(mmu.registers.sp);
//     debugRegisterPC.innerText = formatHex(mmu.registers.pc);
//     debugGPUFlagBGEnabled.innerText = gpu.flagIsSet(GPU_FLAG_BG_ENABLED) ? "Yes" : "No";
//     debugGPUFlagSpritesEnabled.innerText = gpu.flagIsSet(GPU_FLAG_SPRITES_ENABLED) ? "Yes" : "No";
//     debugGPUFlagSpriteSize.innerText = gpu.flagIsSet(GPU_FLAG_SPRITE_SIZE) ? "8x16" : "8x8";
//     debugGPUFlagBGTileMap.innerText = gpu.flagIsSet(GPU_FLAG_BG_TILE_MAP) ? "1" : "0";
//     debugGPUFlagBGTileSet.innerText = gpu.flagIsSet(GPU_FLAG_BG_TILE_SET) ? "1" : "0";
//     debugGPUFlagWindowEnabled.innerText = gpu.flagIsSet(GPU_FLAG_WINDOW_ENABLED) ? "Yes" : "No";
//     debugGPUFlagWindowTileMap.innerText = gpu.flagIsSet(GPU_FLAG_WINDOW_TILE_MAP) ? "1" : "0";
//     debugGPUFlagDisplayEnabled.innerText = gpu.flagIsSet(GPU_FLAG_DISPLAY_ENABLED) ? "Yes" : "No";
//     gpu.drawTileSet(0, debugTileset0Ctx);
//     gpu.drawTileSet(1, debugTileset1Ctx);
//     if (debugPauseFrame.checked) {
//         emulator.pause();
//     }
// });

// emulator.loadROM(romSelect.value).then(() => {
//     startButton.removeAttribute("disabled");
// });

// window["jsboy"] = emulator;